const utils = require("./_utils");

module.exports = {
  appendMeta,
};

async function appendMeta(filePath, itemId, item) {
  let content = await utils.readFile(filePath);
  if (item) {
    content = content.replace(
      `<title>Lin-shop - Giải khát gì cũng có</title>`,
      `<title>Lin-shop - ${item.name}</title>`
    );
    content = content.replace(
      '<meta property="og:title" content="Lin-shop | đã khát, đặt là ship.">',
      `<meta property="og:title" content="${item.name} - ${formatCurrency(
        item.price
      )}">`
    );
    content = content.replace(
      '<meta property="og:url" itemprop="url" content="https://lin-shop2.herokuapp.com">',
      `<meta property="og:url" itemprop="url" content="https://lin-shop2.herokuapp.com/items/${itemId}">`
    );
    content = content.replace(
      '<meta property="og:image" content="/assets/shopping.png">',
      `<meta property="og:image" content="${item.src}">`
    );
    content = content.replace(
      '<meta itemprop="image" content="/assets/shopping.png">',
      `<meta itemprop="image" content="${item.src}">`
    );
  }

  return content;
}

function formatCurrency(value, separator = ".") {
  if (!value) {
    return "";
  }
  const str = String(value);
  let result = "";
  for (let i = str.length - 1; i >= 0; i--) {
    const j = str.length - i;
    result = (j % 3 === 0 ? separator : "") + str[i] + result;
  }

  if (str.length % 3 === 0) {
    result = result.slice(1);
  }

  result += " vnđ";

  return result;
}
