# LinShop

This project using Angular v9.0.7 & Nodejs v12.16.1

## Development

Run `npm install` at first time
Run `npm run dev` OR `node _server.js` + `ng s --aot` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

`ng build --prod`

## Integrate

Run `host=http://localhost node _server.js` for a dev server. Navigate to `http://localhost/`.

## Deployment (heroku host)

Run `./deploy.bash build log open`. Watting progress done.
