<a name="1.13"></a>

...

<a name="1.14"></a>

# 1.14 (2020-10-26)

### Bug Fixes

- **mailchimp:** resolve new version email sent more time
- **mailchimp:** correct result toast when send all emails

### Features

- **common:** add CHANGELOG.md
- **common:** add new_version_email_template.html
- **utils:** add sleep function
- **mailchimp:** append changelog of version into email to send for all subscribers

### Code Refactoring

- **utils:** add read file async function
- **seo:** implement read file async function

<a name="1.15"></a>

## Bug Fixes

- **cart:** resolve 0 or empty quantity

## Breaking Changes

- **common:** add document.doc
- **category:** display message instead of notification when add 1 item
- **common:** display notification at top right corner

<a name="1.16"></a>

## Features

- **common:** handle loading state
- **common:** find item by name
- **page:** add find result page
- **item-info:** show breadcrumb

## Code Refactoring

- **list-item:** separate component
- **common:** rename components
- **common:** move categories menu to category-item component

## Breaking Changes

- **items:** append category property

<a name="1.17"></a>

## Features

- **common:** inject version & checking upgrade

<a name="1.18"></a>

## Features

- **common:** user registry page with validation
- **common:** handle authorization
- **common:** show user infomation & update infomation

<a name="1.19"></a>

## Features

- **registry:** validate cmnd, dob
- **user-info:** validate cmnd, dob
- **user-info:** allow user change password
- **common:** log out

<a name="1.20"></a>

## Bug Fixes

- **common:** resolve md5 dependency
- **common:** resolve deployment

<a name="1.21"></a>

## Features

- **payment:** validate delivery infomation
- **payment:** auto set delivery infomation as member information at first time
- **authorize:** increate token length

<a name="1.22"></a>

### Code Refactoring

- **authentication:** logout when token is expired

## Breaking Changes

- **sign-up:** modify aggrement

## Features

- **login:** Auto focus at username when navigate to login page

<a name="1.24"></a>

## Bug fixes

- **payment:** Set default phone number prefix

<a name="1.25"></a>

## Bug Fixes

- **payment:** not show bill infomation & show buy more component
- **payment:** correct Ngan Luong tranfer fee

## Code Refactoring

- **buy-more:** added
- **loading:** optimize loading
- **store:** optimize code

## Features

- **account:** navigate last page after login or logout
- **payment:** apply discount for member with fully information
- **authorize:** auto log out when token expired
- **common:** toggle show news

## Breaking Changes

- **payment:** change payment API

<a name="1.26"></a>

## Bug Fixes

- **user-info:** update fullname when change in user info page
- **payment:** mark require fields for delivery info
- **deploy:** WARN: Heroku will stop & restart many time per day. Move increase version function outside \_server.js

## Breaking Changes

- **login:** set login expired to 15 mins
- **payment:** payment method must be select by user at first time
- **payment:** clear browser data when login / logout / change password / unauthorize

<a name="1.27"></a>

## Features

- **account:** show user info at menu & greeting message

## Bug Fixes

- **common:** remove mailchimp notification

<a name="1.28"></a>

## Bug Fixes

- **login:** resolve wrong user login information

<a name="1.29"></a>

## Breaking Changes

- **not-found-page:** update not found page

<a name="1.30"></a>

## Breaking Changes

- **item:** Ô long -> Trà Ô Long; 7up price 75000 vnd -> 12000
- **login-page:** handle loading state
- **find-item:** can click to find item (include enter)
- **item-info:** set default quantity to 1

## Bug Fixes

- **payment:** correct vnd currency format with decimal value
- **invite:** correct undo when email information invalid (status 400)