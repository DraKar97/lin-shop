/**
 * Config: Embed mailchimp subscrible popup
 * - @see https://us2.admin.mailchimp.com/audience/ : Manage Audience > Signup forms > get Script > check connection
 * - Note: clear cookie, then show again OR Ctrl+Shift+N
 *
 * Integrate:
 * @see https://us2.admin.mailchimp.com/
 * @see https://mailchimp.com/developer/api/
 * @see https://mailchimp.com/developer/guides/marketing-api-quick-start/#generate-your-api-key
 */

const UNIT = 1000; // 1s
const RECHECK_TIME = 30 * UNIT; // 10 s
const BLOCK_REQUEST_CODE_TIME = 15 * 60 * UNIT; // 15 mins
var BLOCK_REQUEST_CODE_CURRENT = 0;

const http = require("./_request");
const db = require("./_firebase");
const md5 = require("./_md5");
const twilio = require("./_twilio");
const utils = require("./_utils");

module.exports = {
  config,
  notifyNewVersion,

  apiGetMembers,
  apiUpdateMember,
  apiUpdateMembers,
  apiSubscribles,

  apiRequestCode,
  apiSendAll,
  apiGetStatus,

  sendAll,
  getStatus,
};

let isProgress = false;
let codeExecute;
const api_key = "0351a1085057ed23d6d9a12bfaddd34c-us2";
const client_id = "774095824165";
const client_secret = "d65092c0629cc42348d0e6b82c18969f9ce420354961854ac8";

/**
 * @see https://us2.admin.mailchimp.com/audience : Manage Audience > Settings > unique id
 * Only allow 1 audience (free account)
 */
const audienceId = "bf3551439a";
/** @see https://us2.admin.mailchimp.com/templates/editor?id=404132 */
const newVersionTemplateId = 404132;
/** @see https://us2.admin.mailchimp.com/templates/editor?id=404048 */
const pingTemplateId = 404048;
var newVersionTemplateFilePath = __dirname + "/new_version_email_template.html";
var changeLogFilePath = __dirname + "/CHANGELOG.md";

const host = "us2.api.mailchimp.com";
const path = "/3.0";
const auth =
  "Basic " + Buffer.from(`caigicungduoc:${api_key}`).toString("base64");

const PATH_AUDIENCE = `${path}/lists`;
const PATH_MEMBERS = `${path}/lists/${audienceId}/members`;
const PATH_CAMPAIGN = `${path}/campaigns`;
const PATH_TEMPLATE = `${path}/templates`;

const STATUS = {
  SUBSCRIBED: "subscribed",
  UNSUBSCRIBED: "unsubscribed",
  PENDING: "pending",
  CLEANED: "cleaned",
  /** To add */
  TRANSACTIONAL: "transactional",
  ERROR: 400,
};

const SEND_ALL_MODE = {
  NEW_VERSION: "new-version",
  PING: "ping",
};

async function apiSubscribles(req, res) {
  const { userInfos } = req.body;
  if (!userInfos) {
    res.status(400).end();
    return;
  }

  let result = [];
  for (let userInfo of userInfos) {
    const res = await onSubscrible(userInfo);
    db.addSubscriber({ ...userInfo, errors: res.errors });
    result.push(res);
  }
  res.send(result);
}

async function apiGetMembers(req, res) {
  const members = await getMembers();
  res.send(members);
}

async function apiUpdateMember(req, res) {
  const { userInfo } = req.body;
  if (!userInfo) {
    res.status(400).end();
    return;
  }

  const newUserInfo = await updateMember(userInfo);
  res.send(newUserInfo);
}

async function apiUpdateMembers(req, res) {
  const { userInfos } = req.body;
  if (!userInfos) {
    res.status(400).end();
    return;
  }
  // setTimeout(async () => {
  const newUserInfos = await updateMembers(userInfos);
  res.send(newUserInfos);
  // }, Math.round(Math.random() * 2000));
}

async function apiRequestCode(req, res) {
  if (BLOCK_REQUEST_CODE_CURRENT > 0) {
    res.send({ error: "Tạm khóa tính năng yêu cầu lấy mã thực thi" });
    return;
  }

  codeExecute = `CODE-${Math.round(Math.random() * 100000)}`;
  const twilioRes = await twilio.sendSms(
    `Ai đó vừa yêu cầu lấy mã thực thi cho ứng dụng Lin-shop. Mã thực thi là ${codeExecute}`
  );
  BLOCK_REQUEST_CODE_CURRENT = Number(BLOCK_REQUEST_CODE_TIME);
  const interval = setInterval(() => {
    BLOCK_REQUEST_CODE_CURRENT > 0
      ? (BLOCK_REQUEST_CODE_CURRENT -= UNIT)
      : clearInterval(interval);
  }, UNIT);

  res.send(twilioRes);
}

async function apiSendAll(req, res) {
  if (req.body.code !== codeExecute) {
    res.status(400).send("Sai mật khẩu");
    return;
  }

  if (isProgress) {
    res.status(400).send(getStatus());
    return;
  }

  const result = await sendAll(SEND_ALL_MODE.PING);
  res.send(result);
}

function apiGetStatus(req, res) {
  res.send(getStatus());
}

/**
 * Get Emails by Audience
 * email_address, status, merge_fields{FNAME,LNAME,ADDRESS,PHONE,BIRTHDAY}, id, unique_email_id, email_type
 */
async function getMembers() {
  const { members } = await http.get(host, PATH_MEMBERS, auth);
  return members;
}

async function onSubscrible(
  userInfo = {
    email: "daoxiang97@gmail.com",
    firstName: "Nhật Linh",
    lastName: "Huỳnh",
    birthday: "",
    phone: "",
  }
) {
  const res = await http.post(
    host,
    PATH_MEMBERS,
    auth,
    JSON.stringify({
      email_address: userInfo.email,
      status: STATUS.SUBSCRIBED,
      merge_fields: {
        FNAME: userInfo.firstName,
        LNAME: userInfo.lastName,
        BIRTHDAY: userInfo.birthday,
        PHONE: userInfo.phone,
      },
    }),
    "utf8"
  );
  if (res.errors || res.email_address !== userInfo.email) {
    console.error("--- subscrible =", res.errors || JSON.parse(res).detail);
  }

  return {
    errors: res.errors || JSON.parse(res).detail,
    email: res.email_address || userInfo.email,
  };
}

async function updateMember(userInfo) {
  const { email_address } = userInfo;
  if (!validateEmail(email_address)) {
    return { status: STATUS.ERROR };
  }

  const email = md5(email_address);

  const req = JSON.stringify({
    ...userInfo,
    status_if_new: STATUS.SUBSCRIBED,
  });
  const res = await http.put(
    host,
    `${PATH_AUDIENCE}/${audienceId}/members/${email}`,
    auth,
    req
  );
  return res;
}

async function updateMembers(userInfos) {
  let result = [];
  for (let userInfo of userInfos) {
    const res = await updateMember(userInfo);
    const { email_address, merge_fields, status } = res;
    if (status !== STATUS.ERROR) {
      db.changeSubscriber(userInfo);
    }
    result.push({ id: userInfo.id, email_address, merge_fields, status });
  }
  return result;
}

async function createCampaign(
  template_id,
  settings = {
    title:
      "Gửi mail cho toàn bộ người theo dõi vào lúc " +
      db.getTime().toLocaleString(),

    subject_line: "Giới thiệu",
    preview_text: "Hãy truy cập Lin-shop nhé",
    from_name: "Lin-shop",
    reply_to: "daoxiang97@gmail.com",
    auto_footer: false,
  }
) {
  const req = JSON.stringify({
    type: "regular",
    recipients: {
      list_id: audienceId,
    },
    settings: { ...settings, template_id },
  });
  const res = await http.post(host, PATH_CAMPAIGN, auth, req);

  return res;
}

async function createTemplate(name, filePath) {
  const html = await utils.readFile(filePath);
  const req = JSON.stringify({
    name,
    html,
  });
  const res = await http.post(host, PATH_TEMPLATE, auth, req);
  return res;
}

async function updateNewVersionTemplate(
  newFeature,
  id = newVersionTemplateId,
  filePath = newVersionTemplateFilePath
) {
  const name = `Báo cập nhật phiên bản v${await db.getVersion()}`;
  let html = await utils.readFile(filePath);
  if (newFeature) {
    html = html.replace(
      "Hãy truy cập để trải nghiệm tính năng mới",
      newFeature
    );
  }

  const req = JSON.stringify({
    name,
    html,
  });
  const res = await http.patch(host, `${PATH_TEMPLATE}/${id}`, auth, req);
  return res;
}

async function getCampaign(campaignId) {
  const res = await http.get(host, `${PATH_CAMPAIGN}/${campaignId}`, auth);

  return res;
}

async function replicateCampaign(campaignId) {
  const res = await http.post(
    host,
    `${PATH_CAMPAIGN}/${campaignId}/actions/replicate`,
    auth,
    null
  );

  return res.id;
}

async function modifyCampaign(
  campaignId,
  settings = {
    title:
      "Gửi mail cho toàn bộ người theo dõi vào lúc " +
      db.getTime().toLocaleString(),

    subject_line: "Giới thiệu",
    preview_text: "Hãy truy cập Lin-shop nhé",
    from_name: "Lin-shop",
    reply_to: "daoxiang97@gmail.com",
    auto_footer: false,
  }
) {
  const data = JSON.stringify({
    settings,
  });
  const res = http.patch(host, `${PATH_CAMPAIGN}/${campaignId}`, auth, data);

  return res;
}

async function startCampaign(campaignId) {
  const res = await http.post(
    host,
    `${PATH_CAMPAIGN}/${campaignId}/actions/send`,
    auth
  );

  return res;
}

// async function stopCampaign(campaignId) {
//   const res = await http.post(
//     host,
//     `${PATH_CAMPAIGN}/${campaignId}/actions/`
//   );

//   return res;
// }

async function deleteCampaign(campaignId) {
  const res = http.delete(host, `${PATH_CAMPAIGN}/${campaignId}`, auth);

  return res;
}

async function sendAll(mode = SEND_ALL_MODE.NEW_VERSION, settings, content) {
  isProgress = true;

  console.log("--- send mails with mode =", mode);

  let campaignId;
  if (mode === SEND_ALL_MODE.NEW_VERSION) {
    const { id: templateId } = await updateNewVersionTemplate(content);
    console.log("--- template =", templateId);
    campaignId = (await createCampaign(templateId, settings)).id;
  } else if (mode === SEND_ALL_MODE.PING) {
    campaignId = (await createCampaign(pingTemplateId, settings)).id;
  }

  if (!campaignId) {
    return { error: "Khởi tạo mail thất bại" };
  }
  console.log("--- create =", campaignId);

  const isStart = (await startCampaign(campaignId)) === 204;
  console.log("--- start =", isStart);
  if (!isStart) {
    return { error: "Không thể bắt đầu tiến trình" };
  }

  const interval = setInterval(async () => {
    const info = await getCampaign(campaignId);
    const { status, emails_sent } = info;

    // status === "sending"
    if (status !== "sent") {
      console.log("...");
    } else {
      console.log("--- done");

      clearInterval(interval);
      const isDelete = (await deleteCampaign(campaignId)) === 204;
      console.log("--- delete =", isDelete);
      isProgress = false;
    }
  }, RECHECK_TIME);

  return {
    isStart,
  };
}

function validateEmail(email) {
  if (!email || !email.length) {
    return false;
  }

  const atPos = email.indexOf("@");
  if (atPos < 1) {
    return false;
  }

  const dotPos = email.indexOf(".", atPos + 2);
  if (dotPos === -1) {
    return false;
  }

  const endPost = email.length + 1 >= dotPos;
  return endPost;
}

function getStatus() {
  return { busy: isProgress };
}

async function getChangeLog(version) {
  let content = String(await utils.readFile(changeLogFilePath));

  const nextVersion = (version * 100 + 1) / 100;
  const beginText = `<a name="${version}"></a>`;
  const endText = `<a name="${nextVersion}"></a>`;
  const beginPos = content.indexOf(beginText) + beginText.length;
  const endPos = content.indexOf(endText) - 1;
  content = content.slice(beginPos, endPos > 0 ? endPos : undefined);
  content = content.replace(/\n/g, "<br>");
  return content;
}

async function notifyNewVersion() {
  const version = await db.increaseVersion();
  const changeLog = await getChangeLog(version);

  return sendAll(
    "new-version",
    {
      title: `Thông báo cập nhật v${version} lúc ${db
        .getTime()
        .toLocaleString()}`,

      subject_line: `Lin-shop vừa cập nhật phiên bản ${version}`,
      preview_text: "Hãy truy cập để trải nghiệm",
      from_name: "Lin-shop",
      reply_to: "daoxiang97@gmail.com",
      auto_footer: false,
    },
    changeLog
  );
}

function config(v) {
  newVersionTemplateFilePath = v.root + "/new_version_email_template.html";
  changeLogFilePath = v.root + "/CHANGELOG.md";
  return this;
}
