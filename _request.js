var isWriteLog = false;

const https = require("https");
const httpRequest = require("request");

module.exports = {
  enableLog,

  request,
  get: _get,
  post: _post,
  put: _put,
  patch: _patch,
  delete: _delete,

  get2: _get2,
  post2: _post2,
};

function _request(
  host,
  path,
  auth,
  data,
  callback,
  method = "POST",
  responseType = "json"
) {
  if (isWriteLog) {
    console.log(`[${method}] ${host}${path}`);
  }
  let requestOptions = {
    method,
    host,
    path,
    headers: {
      Acept: `application/${responseType}`,
      "Content-type": "application/x-www-form-urlencoded",
      //"Content-Length": data.length,
      //Authorization: auth,
    },
  };

  if (auth) {
    requestOptions.headers["Authorization"] = auth;
  }
  if (method === "POST") {
    requestOptions.headers["Content-type"] = `application/${responseType}`;
  }
  if (data) {
    requestOptions.headers["Content-Length"] = Buffer.byteLength(data);
  }

  const req = https.request(requestOptions, (res) => {
    let body = "";
    res.setEncoding("utf8");
    res.on("data", (part) => {
      body += part;
    });

    res.on("end", () => {
      const str = body.toString();
      try {
        const _res = responseType === "json" ? JSON.parse(str) : str;
        callback(_res, res.statusCode);
      } catch (ex) {
        // console.log("--- error =", ex);
        // console.log("--- res =", str || res.statusCode);
        callback(res.statusCode, res.statusCode);
      }
    });
  });

  if (data) {
    req.write(data);
  }

  req.end();
}

function request(host, path, auth, data, method = "POST", responseType) {
  return new Promise((_) =>
    _request(host, path, auth, data, _, method, responseType)
  );
}

async function _get(host, path, auth, responseType) {
  return await request(host, path, auth, null, "GET", responseType);
}

async function _post(host, path, auth, data, responseType) {
  return await request(host, path, auth, data, "POST", responseType);
}

async function _put(host, path, auth, data, responseType) {
  return await request(host, path, auth, data, "PUT", responseType);
}

async function _patch(host, path, auth, data, responseType) {
  return await request(host, path, auth, data, "PATCH", responseType);
}

async function _delete(host, path, auth) {
  return await request(host, path, auth, null, "DELETE");
}

function _get2(url, auth) {
  return new Promise((_) => {
    httpRequest.get(
      url,
      {
        headers: {
          Authorization: auth,
          "Content-Type": "application/x-www-form-urlencoded",
        },
      },
      (err, res) => {
        _(res.body);
      }
    );
  });
}

function _post2(url, auth, form) {
  return new Promise((_) => {
    httpRequest.post(
      url,
      {
        headers: {
          Authorization: auth,
          "Content-Type": "application/x-www-form-urlencoded",
        },
        form,
      },
      (err, res) => {
        if (err) {
          console.log("--- _post2 error = ", err);
        }
        _(res.body);
      }
    );
  });
}

function enableLog() {
  isWriteLog = true;
}
