import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NzModalRef, NzModalService } from "ng-zorro-antd/modal";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { ChangePasswordModalComponent } from "src/app/components/change-password-modal/change-password-modal.component";
import { UserInfo } from "src/app/models/user.model";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "l-user-info-name",
  templateUrl: "user-info.component.html",
  styleUrls: ["user-info.component.less"],
})
export class UserInfoComponent implements OnInit, OnDestroy {
  validateForm!: FormGroup;
  init = false;
  loading = false;

  modalRef: NzModalRef;

  get formData(): UserInfo {
    return this.validateForm.getRawValue();
  }

  constructor(
    private fb: FormBuilder,
    private noti: NzNotificationService,
    private user: UserService,
    private modal: NzModalService
  ) {}

  ngOnInit() {
    this.user.getUserInfo().subscribe((userInfo) => {
      this.init = true;
      this.render(userInfo);
    });
  }

  ngOnDestroy() {
    if (this.modalRef) {
      this.modalRef.destroy();
    }
  }

  submitForm(): void {
    if (this.validateForm.valid) {
      this.loading = true;
      this.validateForm.disable();
      let userInfo = this.formData;
      this.user
        .update(userInfo)
        .subscribe(this.updateSuccess, this.updateError);
      return;
    }

    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  render(userInfo: UserInfo): void {
    this.validateForm = this.fb.group({
      email: [userInfo.email],
      fullName: [userInfo.fullName],
      phoneNumberPrefix: [userInfo.phoneNumberPrefix],
      phoneNumber: [userInfo.phoneNumber, [Validators.minLength(10)]],
      address: [userInfo.address],
      cmnd: [userInfo.cmnd, [Validators.pattern(/^\d+$/)]],
      dob: [userInfo.dob],
    });
  }

  updateSuccess = (userInfo: UserInfo) => {
    this.noti.success("Cập nhật thông tin thành công", "");
    this.validateForm.enable();
    window.requestAnimationFrame(() => {
      this.render(userInfo);
    });
    this.loading = false;
  };

  updateError = () => {
    this.noti.error("Cập nhật thông tin thất bại", "");
    this.validateForm.enable();
    this.loading = false;
  };

  onChangePassword() {
    this.modalRef = this.modal.confirm({
      nzTitle: "Đổi mật khẩu",
      nzContent: ChangePasswordModalComponent as any,
      nzOkText: null,
      nzCancelText: null,
    });
  }
}
