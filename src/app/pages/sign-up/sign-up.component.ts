import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { timer } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { SignupFormModel } from "src/app/models/user.model";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "l-sign-up",
  templateUrl: "sign-up.component.html",
  styleUrls: ["sign-up.component.less"],
})
export class SignUpComponent implements OnInit {
  validateForm!: FormGroup;
  loading = false;

  get formData(): SignupFormModel {
    return this.validateForm.getRawValue();
  }

  get isAgree(): boolean {
    return this.formData.agree;
  }

  constructor(
    private fb: FormBuilder,
    private noti: NzNotificationService,
    private user: UserService,
    private router: Router
  ) {}

  submitForm(): void {
    if (this.validateForm.valid) {
      if (!this.isAgree) {
        this.noti.error(
          "Bạn chưa đồng ý với điều khoản",
          "Hãy đọc qua điều khoản của Lin-shop và đánh dấu đồng ý"
        );
        return;
      }

      this.loading = true;
      this.validateForm.disable();
      let userInfo = this.formData;
      delete userInfo.agree;
      delete userInfo.checkPassword;
      this.user
        .registry(userInfo)
        .subscribe(this.registrySuccess, this.registryError);
      return;
    }

    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() =>
      this.validateForm.controls.checkPassword.updateValueAndValidity()
    );
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [
        null,
        [Validators.email, Validators.required],
        [this.emailAsyncValidator],
      ],
      password: [null, [Validators.required]],
      checkPassword: [null, [Validators.required, this.confirmationValidator]],
      fullName: [null],
      phoneNumberPrefix: ["+84"],
      phoneNumber: [null, [Validators.minLength(10)]],
      address: [null],
      cmnd: [null, [Validators.pattern(/^\d+$/)]],
      dob: [null],
      agree: [false],
    });
  }

  emailAsyncValidator = (control: FormControl) =>
    timer(1000).pipe(
      switchMap(() =>
        this.user
          .checkExist(control.value)
          .pipe(map((exist) => (exist ? { duplicated: true } : {})))
      )
    );

  registrySuccess = () => {
    this.noti.success(
      "Đăng kí thành công",
      "Chúc mừng bạn đã trở thành thành viên của Lin-shop"
    );
    this.router.navigate(["/login"]);
  };

  registryError = () => {
    this.noti.error(
      "Lỗi đăng kí thành viên",
      "Hãy thử lại với thông tin khác hoặc liên hệ quản trị"
    );
    this.loading = false;
  };
}
