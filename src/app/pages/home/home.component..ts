import { Component, OnInit } from "@angular/core";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { map } from "rxjs/operators";
import { MailchimpService } from "src/app/services/mailchimp.service";

@Component({
  selector: "l-home",
  templateUrl: "home.component.html",
  styleUrls: ["home.component.less"],
})
export class HomeComponent implements OnInit {
  emails: string[];

  constructor(
    private mailchimp: MailchimpService,
    private noti: NzNotificationService
  ) {}

  ngOnInit() {
    this.mailchimp
      .getMembers()
      .pipe(map((members) => members.map((member) => member.email_address)))
      .subscribe((emails) => {
        this.emails = emails;
      });
  }

  showSignUpForm() {
    if (document.cookie.indexOf("MC") > -1) {
      this.noti.warning(
        "Ai đó đã dùng trình duyệt này đăng kí Lin-shop rồi",
        `<div>1. Hãy thử xóa cookies</div><div>2. Hoặc đăng kí thông qua <a href="/invite">giới thiệu</a></div>`,
        { nzDuration: 5000 }
      );
    }
    this.mailchimp.show();
  }
}
