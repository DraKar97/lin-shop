import { Component, OnInit } from "@angular/core";
import Handsontable from "handsontable";
import { HotTableRegisterer } from "@handsontable/angular";
import { MailchimpService } from "src/app/services/mailchimp.service";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { NzModalService } from "ng-zorro-antd/modal";
import { NzMessageService } from "ng-zorro-antd/message";
import { RequestCodeModalComponent } from "src/app/components/request-code-modal/request-code-modal.component";
import {
  MailchimpUserInfo,
  MailchimpUserInfoSchema,
  MailchimpUserStatus,
  SendAllResponse,
} from "src/app/models/mailchimp.model";
import {
  CellChange,
  EMAIL_COL_PROP,
  InviteListColumnSettings,
  MembersChangeObj,
  STATUS_COL_PROP,
} from "./invite.model";

@Component({
  selector: "l-invite",
  templateUrl: "invite.component.html",
  styleUrls: ["invite.component.less"],
})
export class InviteComponent implements OnInit {
  /** Loading members list */
  loading = true;
  /** Count request pending */
  saving = 0;
  /** Send-all method in progress */
  isBusy = true;
  infoNotiRef: { messageId };

  data: MailchimpUserInfo[];

  hot: Handsontable;
  hotId = "hihi";
  hotReg = new HotTableRegisterer();

  settings: Handsontable.GridSettings = {
    licenseKey: "non-commercial-and-evaluation",
    rowHeights: 30,

    manualColumnResize: true,
    columnHeaderHeight: 40,
    columns: InviteListColumnSettings,
    dataSchema: MailchimpUserInfoSchema,
    minSpareRows: 1,
  };

  constructor(
    private mailchimp: MailchimpService,
    private noti: NzNotificationService,
    private mess: NzMessageService,
    private modal: NzModalService
  ) {}

  ngOnInit() {
    this.getMembers();
    // TODO: replace code by realtime for update status
    this.getSendAllStatus();
  }

  afterInit = () => {
    this.hot = this.hotReg.getInstance(this.hotId);
  };

  render = (members: MailchimpUserInfo[]) => {
    const emailCol = this.hot.propToCol(EMAIL_COL_PROP);
    this.data = members.map((member, index) => ({
      ...member,
      id: `${index + 1}`,
    }));

    this.hot.updateSettings({
      data: [...this.data],
      cells: (row, col) => {
        return row < this.data.length && col === emailCol
          ? { readOnly: true }
          : {};
      },
    });

    this.loading = false;
  };

  beforeChange = (changes: CellChange, source) => {
    if (source === "auto") {
      return;
    }
    let rowsIdChanged = {};
    changes.forEach(([row]) => {
      const { merge_fields, email_address } = this.hot.getSourceDataAtRow(
        row
      ) as any;
      rowsIdChanged[row] = { merge_fields: { ...merge_fields }, email_address };
    });

    changes.forEach(([row, path, oldValue, newValue]) => {
      if (oldValue === newValue) {
        return;
      }

      const [lv1, lv2] = path.split(".");
      if (lv1 != null && lv2 != null) {
        rowsIdChanged[row][lv1][lv2] = newValue;
      } else if (lv1 != null) {
        rowsIdChanged[row][lv1] = newValue;
      }
    });

    if (this.saving === 0) {
      this.mess.remove();
      this.mess.loading("Đang lưu ...", { nzDuration: -1 });
    }
    this.saving++;

    this.mailchimp
      .updateMembers(this.buildMembersRequest(rowsIdChanged))
      .subscribe(this.saveResponse);
  };

  buildNewChange(changes: CellChange[]): MembersChangeObj {
    let newChange: MembersChangeObj = {};
    changes.forEach(([row]) => {
      const { merge_fields, email_address } = this.hot.getSourceDataAtRow(
        row
      ) as any;
      newChange[row] = { merge_fields: { ...merge_fields }, email_address };
    });

    changes.forEach(([row, path, oldValue, newValue]) => {
      if (oldValue === newValue) {
        return;
      }

      const [lv1, lv2] = path.split(".");
      if (lv1 != null && lv2 != null) {
        newChange[row][lv1][lv2] = newValue;
      } else if (lv1 != null) {
        newChange[row][lv1] = newValue;
      }
    });

    return newChange;
  }

  buildMembersRequest(data: MembersChangeObj): MailchimpUserInfo[] {
    let userInfos = Object.keys(data).map((key) => ({
      id: key,
      email_address: data[key].email_address,
      merge_fields: {
        FNAME: data[key].merge_fields.FNAME || "",
        LNAME: data[key].merge_fields.LNAME || "",
        BIRTHDAY: data[key].merge_fields.BIRTHDAY || "",
        PHONE: data[key].merge_fields.PHONE || "",
      },
    }));

    return userInfos;
  }

  onSendAll() {
    let ref = this.modal.confirm({
      nzTitle: "Gửi mail cho toàn bộ người đăng kí",
      nzContent: RequestCodeModalComponent as any,
      nzOkText: null,
      nzCancelText: null,
    });

    ref.afterClose.subscribe(this.confirmRequestCode);
  }

  confirmRequestCode = (code: string) => {
    if (code == null) {
      return;
    }

    this.infoNotiRef = this.noti.info(
      "Đang gửi mail ...",
      "Hãy chờ trong giây lát",
      { nzDuration: 5000 }
    );

    this.mailchimp
      .sendAll(code)
      .subscribe(this.sendAllResponse, this.sendAllError);
  };

  getMembers() {
    this.mailchimp.getMembers().subscribe(this.render, this.unknowError);
  }

  getSendAllStatus() {
    this.mailchimp.getStatus().subscribe(({ busy }) => {
      this.isBusy = busy;
      if (busy) {
        this.mess.loading("Đang gửi mail ...", {
          nzDuration: 60000,
        });
      }
    }, this.unknowError);
  }

  saveResponse = (userInfos: MailchimpUserInfo[]) => {
    let hasError = [];

    const emailCol = this.hot.propToCol(EMAIL_COL_PROP);

    userInfos.forEach((userInfo) => {
      const { status, id } = userInfo;
      const row = parseInt(id, 10);
      if (status === MailchimpUserStatus.ERROR) {
        hasError.push(row + 1);
      } else {
        this.hot.setCellMeta(row, emailCol, "readOnly", true);
      }
      this.renderStatus(row, status);
    });

    if (hasError.length) {
      this.noti.error(
        `Lỗi dòng ${hasError.join(", ")}`,
        "Hãy kiểm tra lại dữ liệu"
      );
    }
    this.saving--;

    if (this.saving === 0) {
      this.mess.remove();
      this.mess.success("Đã lưu");
    }
  };

  sendAllResponse = ({ isStart, error }: SendAllResponse) => {
    if (isStart) {
      this.noti.success(
        "Bắt đầu tiến trình gửi mail",
        "Hãy chờ trong vài phút"
      );
    } else if (error) {
      this.noti.error("Lỗi gửi mail", error);
    }
  };

  sendAllError = ({ error }) => {
    this.noti.remove(this.infoNotiRef.messageId);
    if (error === "Sai mật khẩu") {
      this.noti.error(
        "Sai mật khẩu",
        `Tạm khóa tính năng lấy mã thực thi. Hãy <a href="https://m.me/drakar97" target="_blank">liên hệ quản trị!</a>`,
        { nzDuration: 5000 }
      );
    } else if (error.busy) {
      this.noti.error(
        "Lỗi gửi mail",
        "Máy chủ đang bận. Hãy thử lại sau 1 phút"
      );
    } else {
      this.noti.error(
        "Lỗi gửi mail",
        `Máy chủ gặp lỗi không xác định. Hãy <a href="https://m.me/drakar97" target="_blank">liên hệ quản trị!</a>`,
        { nzDuration: 5000 }
      );
    }
  };

  unknowError = (error) => {
    this.noti.error("Lỗi không xác định", error && error.message, {
      nzDuration: 5000,
    });
  };

  renderStatus(row: number, status: MailchimpUserStatus) {
    const statusCol = this.hot.propToCol(STATUS_COL_PROP);
    const TD = this.hot.getCell(row, statusCol);
    if (status === MailchimpUserStatus.ERROR) {
      this.hot.setCellMeta(row, statusCol, "error", true);
    } else {
      this.hot.removeCellMeta(row, statusCol, "error");
    }

    const meta = this.hot.getCellMeta(row, statusCol);
    const renderer = this.hot.getCellRenderer(row, statusCol);
    if (renderer) {
      renderer.apply(this, [
        this.hot,
        TD,
        row,
        statusCol,
        STATUS_COL_PROP,
        status,
        meta,
      ]);
    }

    const totalCol = this.hot.countCols();
    for (let col = 0; col < totalCol; col++) {
      let TD = this.hot.getCell(row, col);
      if (TD) {
        if (status === MailchimpUserStatus.ERROR) {
          TD.className = "error";
        } else {
          TD.className = "";
        }
      }
    }
  }
}
