import Handsontable from "handsontable";
import { STATUS_COL_PROP } from "./invite.model";

export const textAutoRender = (
  hot,
  TD,
  row,
  col,
  prop,
  value,
  meta,
  ...args
) => {
  Handsontable.dom.empty(TD);
  if (!value) {
    value = meta.newValue.apply(this, [row]);
  }
  Handsontable.renderers.TextRenderer.apply(this, [
    hot,
    TD,
    row,
    col,
    prop,
    value,
    meta,
    ...args,
  ]);

  const statusCol = hot.propToCol(STATUS_COL_PROP);
  const error = hot.getCellMeta(row, statusCol).error;
  if (error) {
    TD.className = "error";
  }
};

export const textRender = (
  hot: Handsontable,
  TD: HTMLElement,
  row,
  col,
  prop,
  value,
  meta,
  ...args
) => {
  Handsontable.dom.empty(TD);
  Handsontable.renderers.TextRenderer.apply(this, [
    hot,
    TD,
    row,
    col,
    prop,
    value,
    meta,
    ...args,
  ]);

  const statusCol = hot.propToCol(STATUS_COL_PROP);
  const error = hot.getCellMeta(row, statusCol).error;
  if (error) {
    TD.className = "error";
  }
};
