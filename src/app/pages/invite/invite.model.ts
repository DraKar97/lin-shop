import Handsontable from "handsontable";
import { MailchimpUserInfo } from "src/app/models/mailchimp.model";
import { textAutoRender, textRender } from "./render.model";

/** [rowIndex, colProp, oldValue, newValue] */
export type CellChange = [number, string, any, any];

export type MembersChangeObj = { [rowIndex: string]: MailchimpUserInfo };

export const STATUS_COL_PROP = "status";
export const EMAIL_COL_PROP = "email_address";
export const InviteListColumnSettings: Handsontable.ColumnSettings[] = [
  {
    title: "STT",
    data: "id",
    readOnly: true,
    renderer: textAutoRender,
    newValue: (row) => row + 1,
    width: 50,
  },
  {
    title: "Email",
    data: EMAIL_COL_PROP,
    renderer: textRender,
    width: 250,
  },
  {
    title: "Họ",
    data: "merge_fields.FNAME",
    renderer: textRender,
    width: 200,
  },
  {
    title: "Tên",
    data: "merge_fields.LNAME",
    renderer: textRender,
    width: 200,
  },
  {
    title: "Ngày sinh</br>(MM/DD)",
    data: "merge_fields.BIRTHDAY",
    renderer: textRender,
    width: 100,
  },
  {
    title: "Số điện thoại",
    data: "merge_fields.PHONE",
    renderer: textRender,
    width: 100,
  },
  {
    title: "Trạng thái",
    data: STATUS_COL_PROP,
    readOnly: true,
    renderer: textAutoRender,
    newValue: () => "",
  },
];
