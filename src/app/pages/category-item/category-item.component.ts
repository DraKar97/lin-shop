import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivationEnd, NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { filter, map } from "rxjs/operators";
import {
  CategoryEnum,
  getDefaultItemImage,
} from "src/app/models/category.model";
import { ItemModel } from "src/app/models/item.model";
import { SoldItemService } from "src/app/services/sold-item.service";
import { StoreService } from "src/app/services/store.service";

@Component({
  selector: "l-category-item",
  templateUrl: "category-item.component.html",
  styleUrls: ["category-item.component.less"],
})
export class CategoryItemComponent implements OnInit, OnDestroy {
  activeCategory: CategoryEnum;
  init = { listItem: false };
  loading = { listItem: false };
  items: ItemModel[];

  sub: Subscription;

  constructor(
    private router: Router,
    protected store: StoreService,
    private soldItemService: SoldItemService
  ) {}

  ngOnInit() {
    this.initListItem();
    this.sub = this.router.events
      .pipe(filter((e) => e instanceof NavigationEnd))
      .subscribe(this.initListItem);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  initListItem = () => {
    this.loading.listItem = true;
    const url = this.router.url;
    const urlTree = this.router.parseUrl(url);
    const segments = urlTree.root.children.primary.segments;
    const path = segments[0].path;

    if (path === "items") {
      const { name, category } = urlTree.queryParams;

      this.soldItemService
        .getItemsByName(name, category)
        .subscribe(this.getListItemSuccess);
    } else if (path === "categories") {
      this.activeCategory = segments[1].path as any;
      this.soldItemService
        .getItemsByCategory(this.activeCategory)
        .subscribe(this.getListItemSuccess);
      this.store.categoryChanged(this.activeCategory);
    }
  };

  getListItemSuccess = (items: ItemModel[]) => {
    this.items = items;
    this.init.listItem = true;
    this.loading.listItem = false;
  };
}
