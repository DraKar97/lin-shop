import { Component, OnDestroy } from "@angular/core";
import { ActivatedRoute, ActivationEnd, Router } from "@angular/router";
import { NzModalService } from "ng-zorro-antd/modal";
import { combineLatest, Subscription } from "rxjs";
import { filter, map } from "rxjs/operators";
import { PAYMENT_METHOD } from "src/app/models/payment.model";
import { NganLuongService } from "src/app/services/payment-service/ngan-luong.service";
import { PayCodService } from "src/app/services/payment-service/pay-cod.service";
import { PaypalService } from "src/app/services/payment-service/paypal.service";
import { StoreService } from "src/app/services/store.service";

@Component({
  selector: "l-thank-you",
  templateUrl: "thank-you.component.html",
  styleUrls: ["thank-you.component.less"],
})
export class ThankYouComponent implements OnDestroy {
  sub: Subscription;
  error = false;

  constructor(
    private payCodService: PayCodService,
    private paypalService: PaypalService,
    private nganLuongService: NganLuongService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: StoreService,
    private modalService: NzModalService
  ) {
    this.sub = combineLatest([
      router.events
        .pipe(filter((e) => e instanceof ActivationEnd))
        .pipe(map((e: ActivationEnd) => e.snapshot.params["payment-method"])),

      this.activatedRoute.queryParams,
    ]).subscribe(this.onApproveCompleted);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onApproveCompleted = ([method, params]) => {
    switch (method) {
      case PAYMENT_METHOD.COD:
        this.handlePayCodApproved(params);
        break;
      case PAYMENT_METHOD.PAYPAL:
        this.handlePaypalApproved(params);
        break;
      case PAYMENT_METHOD.NGAN_LUONG:
        this.handleNganLuongApproved(params);
        break;
      default:
        this.router.navigate(["/404"]);
        break;
    }
  };

  handlePayCodApproved({ token }) {
    if (!token) {
      alert("Có lỗi gì đó rồi? Hãy liên hệ Linh nhé ;)");
      return;
    }
    this.payCodService.capture(token).subscribe(({ status }) => {
      if (!status) {
        this.showPaymentFailure();
        return;
      }

      if (status === "COMPLETED") {
        this.store.clearCart();
      }
    });
  }

  handlePaypalApproved({ token, PayerID }) {
    if (!token || !PayerID) {
      alert("Có lỗi gì đó rồi? Hãy liên hệ Linh nhé ;)");
      return;
    }
    this.paypalService.capture(token, PayerID).subscribe(({ status }) => {
      if (!status) {
        this.showPaymentFailure();
        return;
      }

      if (status === "COMPLETED") {
        this.store.clearCart();
      }
    });
  }

  handleNganLuongApproved({ error_code, token }) {
    if (!error_code || error_code !== "00" || !token) {
      this.showPaymentFailure();
      return;
    }

    this.nganLuongService.capture(token).subscribe();
    this.store.clearCart();
  }

  showPaymentFailure() {
    this.modalService.error({
      nzTitle: "Thanh toán thất bại",
      nzContent: "Hãy thử thanh toán lại, hoặc liên hệ quản trị",
    });
    this.error = true;
  }
}
