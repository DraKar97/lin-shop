import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { LoginFormModel } from "src/app/models/user.model";
import { getNavTarget, ignoreError } from "src/app/models/utils";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "l-login",
  templateUrl: "login.component.html",
  styleUrls: ["login.component.less"],
})
export class LoginComponent implements OnInit {
  validateForm: FormGroup;
  nav: string;
  loading = false;

  get formData(): LoginFormModel {
    return this.validateForm.getRawValue();
  }

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private user: UserService
  ) {}

  ngOnInit() {
    this.initForm();
    this.nav = "/" + (getNavTarget(this.router) || "");
  }

  initForm() {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [false],
    });
  }

  onLogin() {
    if (this.validateForm.valid) {
      this.loading = true;
      this.user
        .login(this.formData)
        .subscribe(
          this.loginSuccess,
          ignoreError,
          () => (this.loading = false)
        );
      return;
    }

    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  loginSuccess = () => {
    this.router.navigate([this.nav]);
  };
}
