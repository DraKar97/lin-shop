import { Component, OnInit } from "@angular/core";
import { PAYMENT_METHOD } from "src/app/models/payment.model";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { StoreService } from "src/app/services/store.service";
import { PaypalService } from "src/app/services/payment-service/paypal.service";
import { PaymentInfoService } from "src/app/services/payment-info.service";
import { DeliveryInfo } from "src/app/models/delivery-info.model";
import { NganLuongService } from "src/app/services/payment-service/ngan-luong.service";
import { PayCodService } from "src/app/services/payment-service/pay-cod.service";
import { NzModalService } from "ng-zorro-antd/modal";
import { UserInfo } from "src/app/models/user.model";
import { UserService } from "src/app/services/user.service";
import { BillInfo, SimpleCart } from "src/app/models/cart.model";
import { ignoreError } from "src/app/models/utils";
import { BillService } from "src/app/services/payment-service/bill.service";

@Component({
  selector: "l-payment",
  templateUrl: "payment.component.html",
  styleUrls: ["payment.component.less"],
})
export class PaymentComponent implements OnInit {
  simpleCart: SimpleCart;
  init = { billInfo: false, deliveryInfo: false, paymentMethod: false };
  loading = false;

  validateForm!: FormGroup;
  PAYMENT_METHOD = PAYMENT_METHOD;
  paymentMethod: PAYMENT_METHOD;
  billInfo: BillInfo;

  //#region ATM info
  atmNo = "";
  securityCode = "";
  //#endregion

  get deliveryInfo(): DeliveryInfo {
    return this.validateForm.getRawValue();
  }

  set deliveryInfo(v: DeliveryInfo) {
    this.validateForm = this.fb.group({
      email: [v.email, [Validators.email]],
      name: [v.name],
      phonePrefix: [v.phonePrefix || "+84", [Validators.required]],
      phone: [v.phone, [Validators.required, Validators.minLength(10)]],
      address: [v.address, [Validators.required]],
    });
  }

  get isCartEmpty(): boolean {
    return this.simpleCart.length === 0;
  }

  constructor(
    private paymentService: PaymentInfoService,
    private bill: BillService,
    private user: UserService,
    private store: StoreService,
    private fb: FormBuilder,
    private payCodService: PayCodService,
    private paypalService: PaypalService,
    private nganLuongService: NganLuongService,
    private modalService: NzModalService
  ) {}

  ngOnInit() {
    this.simpleCart = this.store.getSimpleCart();
    this.initPaymentMethod();
    this.initDeliveryInfo();
    this.initBillInfo();
    this.user.userLogin$.subscribe(() => {
      this.init.billInfo = false;
      this.initBillInfo();
    });
  }

  initPaymentMethod() {
    this.paymentMethod = this.paymentService.getPaymentMethod();
    this.init.paymentMethod = true;
  }

  initDeliveryInfo() {
    const deliveryInfo = this.paymentService.getDeliveryInfo();
    if (!deliveryInfo) {
      if (this.user.isLogin) {
        this.user
          .getUserInfo()
          .subscribe(
            this.getUserInfoSuccess,
            ignoreError,
            () => (this.init.deliveryInfo = true)
          );
      } else {
        this.deliveryInfo = {
          email: null,
          name: null,
          phone: null,
          phonePrefix: "+84",
          address: null,
        };
        this.init.deliveryInfo = true;
      }
    } else {
      this.deliveryInfo = deliveryInfo;
      this.init.deliveryInfo = true;
    }
  }

  initBillInfo() {
    this.bill.getBillInfo(this.simpleCart).subscribe(this.initBillInfoSuccess);
  }

  initBillInfoSuccess = (billInfo: BillInfo) => {
    this.billInfo = billInfo;
    if (this.paymentMethod === PAYMENT_METHOD.NGAN_LUONG) {
      this.appendNganLuongFee();
    }
    this.init.billInfo = true;
  };

  getUserInfoSuccess = (info: UserInfo) => {
    this.deliveryInfo = {
      email: info.email,
      name: info.fullName,
      phonePrefix: info.phoneNumberPrefix,
      phone: info.phoneNumber,
      address: info.address,
    };
  };

  /** To update validator & fees */
  onPaymentChanged(paymentMethod: PAYMENT_METHOD) {
    let emailCtrl = this.validateForm.controls["email"];
    switch (paymentMethod) {
      case PAYMENT_METHOD.NGAN_LUONG:
        emailCtrl.setValidators([Validators.required, Validators.email]);
        this.appendNganLuongFee();
        break;

      default:
        // case PAYMENT_METHOD.COD:
        // case PAYMENT_METHOD.PAYPAL:
        emailCtrl.setValidators([Validators.email]);
        if (this.billInfo.fees.nganluong) {
          this.removeNganLuongFee();
        }
    }
    emailCtrl.updateValueAndValidity();
  }

  onPay() {
    if (this.validateForm.valid) {
      this.loading = true;
      this.validateForm.disable();
      this.pay();
      return;
    }

    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  pay() {
    if (!this.paymentMethod) {
      this.modalService.error({
        nzTitle: "Bạn chưa chọn phương thức thanh toán",
      });
      this.loading = false;
      this.validateForm.enable();
      return;
    }

    switch (this.paymentMethod) {
      case PAYMENT_METHOD.COD:
        this.payCod();
        break;
      case PAYMENT_METHOD.PAYPAL:
        this.payWithPaypal();
        break;
      case PAYMENT_METHOD.NGAN_LUONG:
        this.payWithNganLuong();
        break;
      default:
        this.modalService.error({
          nzTitle: "Không có phương thức thanh toán này",
          nzContent:
            "<div>Hãy thử phương thức thanh toán khác</div><div>(ví dụ: COD)</div>",
        });
        this.loading = false;
        this.validateForm.enable();
        return;
    }

    this.paymentService.setPaymentMethod(this.paymentMethod);
    this.paymentService.setDeliveryInfo(this.deliveryInfo);
  }

  payCod() {
    this.payCodService
      .pay(this.store.getSimpleCart(), this.deliveryInfo)
      .subscribe(this.createOrderSuccess, this.createOrderFailure);
  }

  payWithPaypal() {
    this.paypalService
      .pay(this.store.getSimpleCart(), this.deliveryInfo)
      .subscribe(this.createOrderSuccess, this.createOrderFailure);
  }

  payWithNganLuong() {
    this.nganLuongService
      .pay(this.store.getSimpleCart(), this.deliveryInfo)
      .subscribe(this.createOrderSuccess, this.createOrderFailure);
  }

  /** Navigate to confirm payment */
  createOrderSuccess = (approveUrl) => {
    window.open(approveUrl, "_parent");
    this.loading = false;
  };

  createOrderFailure = (err) => {
    this.modalService.error({
      nzTitle: "Tạo đơn hàng thất bại",
      nzContent: "Hãy thử lại, hoặc liên hệ quản trị",
    });
    this.loading = false;
    this.validateForm.enable();
  };

  appendNganLuongFee() {
    this.billInfo.fees = Object.assign({}, this.billInfo.fees, {
      nganluong: {
        value: 1760 + this.billInfo.summary / 100,
        description: "Giao dịch",
      },
    });
    this.billInfo.summary += this.billInfo.fees.nganluong.value;
  }

  removeNganLuongFee() {
    let fees = { ...this.billInfo.fees };
    this.billInfo.summary -= this.billInfo.fees.nganluong.value;
    delete fees.nganluong;
    this.billInfo.fees = fees;
  }
}
