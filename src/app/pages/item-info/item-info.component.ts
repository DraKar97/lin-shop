import { Component, OnDestroy } from "@angular/core";
import { ActivationEnd, Router } from "@angular/router";
import { NzMessageService } from "ng-zorro-antd/message";
import { Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import { getDefaultItemImage } from "src/app/models/category.model";
import { ItemInfo } from "src/app/models/item.model";
import { SoldItemService } from "src/app/services/sold-item.service";
import { StoreService } from "src/app/services/store.service";

@Component({
  selector: "l-item-info",
  templateUrl: "item-info.component.html",
  styleUrls: ["item-info.component.less"],
})
export class ItemInfoComponent implements OnDestroy {
  RATES = ["Dở ẹc", "Tạm", "Uống được", "Ngon", "Tuyệt vời"];
  likes = 0;
  dislikes = 0;
  purchaseQuantity = 1;

  loading = true;
  itemId: string;
  item: ItemInfo;

  sub: Subscription;

  constructor(
    private router: Router,
    private soldItemService: SoldItemService,
    private store: StoreService,
    private messageService: NzMessageService
  ) {
    this.sub = this.router.events
      .pipe(filter((e) => e instanceof ActivationEnd))
      .subscribe(this.onItemNavigated);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  get message(): string {
    return `${this.purchaseQuantity} "${this.item.name}" đã được thêm vào giỏ`;
  }

  onItemNavigated = (e: ActivationEnd) => {
    this.loading = true;
    this.itemId = e.snapshot.params.item_id;
    this.getItemInfoById(this.itemId);
  };

  onLike() {
    this.likes++;
  }

  onDislike() {
    this.dislikes++;
  }

  onAddToCart() {
    this.store.addToCart(
      { ...this.item, id: this.itemId },
      this.purchaseQuantity
    );
    this.messageService.success(this.message);
  }

  onNavToCart() {
    this.router.navigate(["/cart"]);
  }

  onAddThenNavToPayment() {
    this.messageService.success(this.message);
    this.store.addToCart(
      { ...this.item, id: this.itemId },
      this.purchaseQuantity
    );
    this.router.navigate(["/payment"]);
  }

  getItemInfoById(id: string) {
    this.soldItemService.getItemById(id).subscribe(
      (item) => {
        this.item = item;
        if (!this.item) {
          return;
        }
        if (!this.item.src) {
          this.item.src = getDefaultItemImage[this.item.category];
        }

        appenRandomInfo(this.item);
        this.store.itemChanged(this.item);
        this.loading = false;

        this.renderFacebookComponent();
      },
      (error) => {
        this.loading = false;
      }
    );
  }

  renderFacebookComponent() {
    const fbCommentComponent = document.querySelector(".fb-comments iframe");
    const fbLikeShareComponent = document.querySelector(".fb-like iframe");

    if (!fbCommentComponent || !fbLikeShareComponent) {
      // waitting loading=false take effect
      window.requestAnimationFrame(() => window["FB"].XFBML.parse());
    }
  }
}

function appenRandomInfo(item) {
  item.description = "cafe 200g, đường 300g, đá 200ml";
  item.rate = [1, 2, 3, 4, 5][Math.round(Math.random() * 100) % 5] as any;
  // item.comments = {
  //   "1": { message: "ngon quá đi thôi" },
  //   "2": { message: "dở ẹc", comments: [{ "1": "ngon mà" }] },
  // };
}
