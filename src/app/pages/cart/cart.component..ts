import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { Cart, PurchaseItem } from "src/app/models/cart.model";
import { ItemInfo } from "src/app/models/item.model";
import { StoreService } from "src/app/services/store.service";

@Component({
  selector: "l-cart",
  templateUrl: "cart.component.html",
  styleUrls: ["cart.component.less"],
})
export class CartComponent implements OnDestroy {
  cart$: Subscription;
  cart: Cart;
  itemIds: string[];
  calc: { [itemId: string]: number; /** cacl all items */ cart: number };
  editingItemId: string;
  oldEditItem: PurchaseItem;

  constructor(private store: StoreService) {
    this.cart$ = store.cart$.subscribe(this.mapCart);
  }

  ngOnDestroy() {
    this.cart$.unsubscribe();
  }

  onHotkey(e: KeyboardEvent, item: PurchaseItem) {
    if (e.key === "Enter") {
      this.updateItemQuantity(item, item.quantity);
      this.editingItemId = null;
    } else if (e.key === "Escape") {
      this.editingItemId = null;
      this.cart[this.oldEditItem.id] = { ...this.oldEditItem };
      this.calcCart(this.cart);
    }
  }

  onHotkey2(e: KeyboardEvent, item: PurchaseItem) {
    if (e.key === "Tab") {
      this.updateItemQuantity(item, item.quantity);
      this.editNextItemOf(item);
    }
  }

  onEditItem(item: PurchaseItem) {
    this.editingItemId = item.id;
    this.oldEditItem = { ...item };
    this.focusItem(item.id);
  }

  onRemoveItem(itemId: string) {
    this.store.removeItem(itemId);
  }

  focusItem(itemId: string, nextRender = true) {
    const func = () => {
      const inputEl: HTMLInputElement = document.querySelector(
        `#purchaseQuantity${itemId} input`
      );
      inputEl.select();
    };

    if (nextRender) {
      window.requestAnimationFrame(func);
    } else {
      func();
    }
  }

  calcCart(cart: Cart) {
    this.calc = { cart: 0 };

    for (let itemId in cart) {
      this.calc[itemId] = cart[itemId].price * cart[itemId].quantity;
      this.calc.cart += this.calc[itemId];
    }
  }

  mapCart = (cart: Cart) => {
    this.cart = cart;
    this.itemIds = Object.keys(cart);
    this.calcCart(cart);
  };

  updateItemQuantity(item: ItemInfo, quantity: number) {
    if (quantity != null && typeof quantity === "number") {
      this.store.updateItemQuantity(item, quantity);
    } else {
      this.store.removeItem(item.id);
    }
    this.editingItemId = null;
  }

  editNextItemOf(item: PurchaseItem) {
    let isFound = false;
    let itemId: string;
    for (itemId in this.cart) {
      if (isFound) {
        break;
      }
      if (itemId === item.id) {
        isFound = true;
      }
    }
    this.editingItemId = itemId;
    this.focusItem(itemId);
  }
}
