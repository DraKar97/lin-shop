import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { CategoryEnum } from "../models/category.model";
import { ItemInfo, ItemModel } from "../models/item.model";

@Injectable({ providedIn: "root" })
export class SoldItemService {
  private readonly API = environment.production
    ? "/api"
    : "http://localhost/api";

  constructor(private http: HttpClient) {}

  getItemsByCategory(category: CategoryEnum): Observable<ItemModel[]> {
    return this.http
      .get<ItemModel[]>(`${this.API}/categories/${category}`)
      .pipe(map((res) => Object.keys(res).map((id) => ({ id, ...res[id] }))));
  }

  getItemsByName(
    name: string,
    category?: CategoryEnum
  ): Observable<ItemModel[]> {
    let params = category ? { name, category } : { name };

    return this.http.get<ItemModel[]>(`${this.API}/items`, { params });
  }

  getItemById(id: string): Observable<ItemInfo> {
    return this.http.get<ItemInfo>(`${this.API}/items/${id}`);
  }
}
