import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { interval, Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { MailchimpUserInfo, RequestCodeResponse, SendAllResponse } from "../models/mailchimp.model";

const subscribleFormId = "PopupSignupForm_0";
const subscribleScriptId = "mcjs";

@Injectable({ providedIn: "root" })
export class MailchimpService {
  protected readonly API = environment.production
    ? "/api"
    : "http://localhost/api";

  constructor(private http: HttpClient) {}

  hide() {
    const form = document.getElementById(subscribleFormId);
    if (form) {
      form.style.display = "none";
    }
  }

  show() {
    const form = document.getElementById(subscribleFormId);
    if (form) {
      form.style.display = "block";
      return;
    }

    window["showMailchimp"]();
    this.customCloseBtn();
  }

  private customCloseBtn() {
    let sub = interval(200).subscribe(() => {
      let closeBtn = document.querySelector(
        `#${subscribleFormId} .mc-closeModal`
      );

      if (!closeBtn) {
        return;
      }

      const parentEl = closeBtn.parentElement;

      const newCloseBtn = document.createElement("div");
      newCloseBtn.className = "mc-closeModal";
      newCloseBtn.onclick = () => {
        const form = document.getElementById(subscribleFormId);
        form.style.display = "none";
      };
      parentEl.replaceChild(newCloseBtn, closeBtn);
      sub.unsubscribe();
    });
  }

  getMembers(): Observable<MailchimpUserInfo[]> {
    return this.http.get<MailchimpUserInfo[]>(`${this.API}/mailchimp/members`);
  }

  updateMembers(
    userInfos: MailchimpUserInfo[]
  ): Observable<MailchimpUserInfo[]> {
    return this.http.put<MailchimpUserInfo[]>(`${this.API}/mailchimp/members`, {
      userInfos,
    });
  }

  subscrible(userInfos: MailchimpUserInfo[]): Observable<MailchimpUserInfo[]> {
    return this.http.post<MailchimpUserInfo[]>(
      `${this.API}/mailchimp/subscrible`,
      {
        userInfos,
      }
    );
  }

  getStatus(): Observable<{ busy: boolean }> {
    return this.http.get<{ busy: boolean }>(`${this.API}/mailchimp/send-all`);
  }

  requestCode(info): Observable<RequestCodeResponse> {
    return this.http.post<RequestCodeResponse>(`${this.API}/mailchimp/send-all`, { info });
  }

  sendAll(code: string): Observable<SendAllResponse> {
    return this.http.put<SendAllResponse>(`${this.API}/mailchimp/send-all`, {
      code,
    });
  }
}
