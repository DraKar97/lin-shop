import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Cart, CART_KEY, SimpleCart } from "../models/cart.model";
import { CategoryEnum } from "../models/category.model";
import { ItemInfo } from "../models/item.model";

@Injectable({ providedIn: "root" })
export class StoreService {
  activeCategory$: BehaviorSubject<CategoryEnum>;
  activeItem$: BehaviorSubject<ItemInfo>;
  cart$: BehaviorSubject<Cart>;

  constructor() {
    this.activeCategory$ = new BehaviorSubject(null);
    this.activeItem$ = new BehaviorSubject(null);
    this.cart$ = new BehaviorSubject(this.getCart());
  }

  get totalItems(): number {
    let result = 0;
    const cart = this.getCart();
    Object.keys(cart).forEach((itemId) => (result += cart[itemId].quantity));
    return result;
  }

  categoryChanged(category: CategoryEnum) {
    this.activeCategory$.next(category);
  }

  itemChanged(item: ItemInfo) {
    this.activeItem$.next(item);
  }

  addToCart(item: ItemInfo, quantity: number): boolean {
    const cart = this.getCart();

    if (cart[item.id]) {
      cart[item.id].quantity += quantity;
    } else {
      cart[item.id] = { ...item, quantity };
    }

    return this.setCart(cart);
  }

  updateItemQuantity(item: ItemInfo, quantity: number): boolean {
    const cart = this.getCart();

    if (cart[item.id]) {
      cart[item.id].quantity = quantity;
    } else {
      cart[item.id] = { ...item, quantity };
    }

    return this.setCart(cart);
  }

  removeItem(itemId: string): boolean {
    const cart = this.getCart();

    delete cart[itemId];

    return this.setCart(cart);
  }

  setCart(cart: Cart): boolean {
    try {
      const cartJson = JSON.stringify(cart);
      localStorage.setItem(CART_KEY, cartJson);
      this.cart$.next({ ...cart });
    } catch (ex) {
      return false;
    } finally {
      return true;
    }
  }

  getCart(): Cart {
    return <Cart>JSON.parse(localStorage.getItem(CART_KEY)) || {};
  }

  clearCart(): boolean {
    return this.setCart({});
  }

  getSimpleCart(): SimpleCart {
    const cart = this.getCart();
    return Object.keys(cart).map((id) => ({ id, quantity: cart[id].quantity }));
  }
}
