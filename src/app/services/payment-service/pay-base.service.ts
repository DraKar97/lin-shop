import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { SimpleCart } from "src/app/models/cart.model";
import { DeliveryInfo } from "src/app/models/delivery-info.model";
import { UserService } from "../user.service";

@Injectable({ providedIn: "root" })
export abstract class PayBaseService {
  protected readonly API = environment.production
    ? "/api/payment"
    : "http://localhost/api/payment";

  constructor(protected http: HttpClient, protected user: UserService) {}
  /** return approve URL */
  abstract pay(cart: SimpleCart, buyerInfo: DeliveryInfo): Observable<string>;
  /** take money & update bill status */
  abstract capture(token, ...args): Observable<{ status }>;
}
