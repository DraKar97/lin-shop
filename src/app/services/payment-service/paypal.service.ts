import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PayBaseService } from "./pay-base.service";
import { SimpleCart } from "src/app/models/cart.model";
import { DeliveryInfo } from "src/app/models/delivery-info.model";

@Injectable({ providedIn: "root" })
export class PaypalService extends PayBaseService {
  private readonly api = `${this.API}/paypal`;

  pay(cart: SimpleCart, buyerInfo: DeliveryInfo): Observable<string> {
    return this.http.post(
      this.api,
      { cart, buyerInfo, ...this.user.email },
      { responseType: "text", ...this.user.authorizeHeader }
    );
  }

  /** To capture money */
  capture(token, PayerID): Observable<{ status: "COMPLETED" }> {
    return this.http.put<{ status: "COMPLETED" }>(this.api, {
      token,
      PayerID,
    });
  }
}
