import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { BillInfo, SimpleCart } from "src/app/models/cart.model";
import { environment } from "src/environments/environment";
import { UserService } from "../user.service";

@Injectable({ providedIn: "root" })
export class BillService {
  protected readonly API = environment.production
    ? "/api/payment"
    : "http://localhost/api/payment";

  constructor(private user: UserService, private http: HttpClient) {}

  getBillInfo(cart: SimpleCart): Observable<BillInfo> {
    return this.http.post<BillInfo>(
      this.API,
      {
        cart,
        ...this.user.email,
      },
      this.user.authorizeHeader
    );
  }
}
