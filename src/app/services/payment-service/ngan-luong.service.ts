import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PayBaseService } from "./pay-base.service";
import { SimpleCart } from "src/app/models/cart.model";
import { DeliveryInfo } from "src/app/models/delivery-info.model";

@Injectable({ providedIn: "root" })
export class NganLuongService extends PayBaseService {
  private readonly api = `${this.API}/ngan-luong`;

  pay(cart: SimpleCart, buyerInfo: DeliveryInfo): Observable<string> {
    return this.http.post(
      this.api,
      { cart, buyerInfo, ...this.user.email },
      { responseType: "text", ...this.user.authorizeHeader }
    );
  }

  /** To update status */
  capture(token: string): Observable<{ status: "COMPLETED" }> {
    return this.http.put<{ status: "COMPLETED" }>(this.api, {
      token,
    });
  }
}
