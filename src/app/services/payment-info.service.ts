import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { DeliveryInfo } from "../models/delivery-info.model";
import { PAYMENT_METHOD } from "../models/payment.model";

const PAYMENT_METHOD_KEY = "payment-method";
const DELIVERY_INFO_KEY = "delivery-info";

@Injectable({ providedIn: "root" })
export class PaymentInfoService {
  protected readonly API = environment.production
    ? "/api"
    : "http://localhost/api";

  constructor(private http: HttpClient) {}

  setPaymentMethod(method: PAYMENT_METHOD) {
    localStorage.setItem(PAYMENT_METHOD_KEY, method);
  }

  getPaymentMethod(): PAYMENT_METHOD {
    return localStorage.getItem(PAYMENT_METHOD_KEY) as PAYMENT_METHOD;
  }

  setDeliveryInfo(info: DeliveryInfo) {
    if (info) {
      localStorage.setItem(DELIVERY_INFO_KEY, JSON.stringify(info));
    }
  }

  getDeliveryInfo(): DeliveryInfo {
    return JSON.parse(localStorage.getItem(DELIVERY_INFO_KEY));
  }

  clearDeliveryInfo() {
    localStorage.removeItem(DELIVERY_INFO_KEY);
  }

  clearPaymentMethod() {
    localStorage.removeItem(PAYMENT_METHOD_KEY);
  }
}
