import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { RssModel, RssPage } from "../models/rss-page.model";

@Injectable({ providedIn: "root" })
export class RssService {
  private readonly API = environment.production
    ? "/api"
    : "http://localhost/api";

  constructor(private http: HttpClient) {}

  /** xml */
  getRss(page: RssPage): Observable<any> {
    return this.http.get(`${this.API}/rss/${page}`, {
      responseType: "text",
    });
  }

  parseRss(page: RssPage): Observable<RssModel> {
    return this.http.get<RssModel>(`${this.API}/rss-parse/${page}`);
  }
}
