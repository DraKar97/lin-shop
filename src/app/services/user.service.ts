import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";
import { environment } from "src/environments/environment";
import {
  SignupFormModel,
  LoginFormModel,
  LoginFormResponse,
  UserInfo,
  ChangePasswordFormModel,
} from "../models/user.model";
import { Router } from "@angular/router";
import * as md5 from "md5";
import { ResponseCode } from "../models/response.model";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { PaymentInfoService } from "./payment-info.service";

const USER_NAME_KEY = "name";
const USER_EMAIL_KEY = "email";
const USER_ACCESS_TOKEN_KEY = "access_token";

@Injectable({ providedIn: "root" })
export class UserService {
  protected readonly API = environment.production
    ? "/api"
    : "http://localhost/api";

  userLogin$: BehaviorSubject<LoginFormResponse>;

  constructor(
    private http: HttpClient,
    private router: Router,
    private noti: NzNotificationService,
    private paymentService: PaymentInfoService
  ) {
    this.userLogin$ = new BehaviorSubject(this.info);
  }

  checkExist(email: string): Observable<boolean> {
    return this.http
      .post(`${this.API}/users/exist`, { email }, { responseType: "text" })
      .pipe(map((exist) => exist === "true"));
  }

  registry(user: SignupFormModel): Observable<never> {
    return this.http.post<never>(`${this.API}/users/signup`, { info: user });
  }

  login(info: LoginFormModel): Observable<LoginFormResponse> {
    const Authorization = md5(`${info.userName}:${info.password}`);
    return this.http
      .post<LoginFormResponse>(
        `${this.API}/users/login`,
        { userName: info.userName, remember: info.remember },
        { headers: { Authorization } }
      )
      .pipe(this.loginSuccess, this.loginFailure);
  }

  logout() {
    return this.http
      .post(`${this.API}/users/logout`, this.email, this.authorizeHeader)
      .pipe(this.logoutSuccess, this.logoutError, this.resolveUnAuthorize);
  }

  // TODO: resolve no 'delivery-info' & expired token
  getUserInfo(): Observable<UserInfo> {
    return this.http
      .post<UserInfo>(
        `${this.API}/users/info`,
        this.email,
        this.authorizeHeader
      )
      .pipe(this.resolveUnAuthorize);
  }

  update(user: UserInfo): Observable<UserInfo> {
    return this.http
      .put<UserInfo>(
        `${this.API}/users/info`,
        { ...this.email, user },
        this.authorizeHeader
      )
      .pipe(this.updateResult, this.resolveUnAuthorize);
  }

  changePassword(info: ChangePasswordFormModel) {
    return this.http
      .put<{ error: string }>(
        `${this.API}/users/change-password`,
        { ...this.email, info },
        this.authorizeHeader
      )
      .pipe(
        this.changePasswordResult,
        this.changePasswordError,
        this.resolveUnAuthorize
      );
  }

  get isLogin(): boolean {
    return !!(this.info && this.info.access_token);
  }

  private set info(info: LoginFormResponse) {
    localStorage[USER_NAME_KEY] = info.userInfo.fullName;
    localStorage[USER_EMAIL_KEY] = info.userInfo.email;
    localStorage[USER_ACCESS_TOKEN_KEY] = info.access_token;
    this.userLogin$.next(this.info);
  }

  private get info(): LoginFormResponse {
    const fullName = localStorage[USER_NAME_KEY];
    const email = localStorage[USER_EMAIL_KEY];
    const access_token = localStorage[USER_ACCESS_TOKEN_KEY];

    if (!email || !access_token) return null;

    return {
      userInfo: {
        fullName,
        email,
      },
      access_token,
    };
  }

  private clearInfo() {
    localStorage.removeItem(USER_NAME_KEY);
    localStorage.removeItem(USER_EMAIL_KEY);
    localStorage.removeItem(USER_ACCESS_TOKEN_KEY);
    this.userLogin$?.next(null);
  }

  get authorizeHeader() {
    const access_token = this.info?.access_token;
    return {
      headers: {
        Authorization: md5(`${access_token}:${environment.clientSecret}`),
      },
    };
  }

  get email() {
    return { email: this.info?.userInfo.email };
  }

  private loginSuccess = tap((res: LoginFormResponse) => {
    this.clearBrowserData();
    this.info = res;
  });

  private loginFailure = catchError(({ error }) => {
    this.noti.error("Đăng nhập thất bại", error);
    throw Error(error);
    return of(null);
  });

  private updateResult = tap((res: UserInfo) => {
    this.info = Object.assign({}, this.info, {
      userInfo: { fullName: res.fullName },
    });
  });

  private resolveUnAuthorize = catchError((res: HttpErrorResponse) => {
    if (res.status === ResponseCode.UNAUTHORIZE) {
      this.clearBrowserData();
      this.noti.error("Xác thực danh tính thất bại", "Hãy đăng nhập lại", {
        nzDuration: 3000,
      });
      this.clearInfo();
      this.router.navigate(["/login"]);
    }
    throw res;
    return of(null);
  });

  private changePasswordResult = tap((hasError?: { error?: string }) => {
    if (hasError) {
      this.noti.error("Đổi mật khẩu thất bại", hasError.error || "");
    } else {
      this.clearBrowserData();
      this.noti.success("Đổi mật khẩu thành công", "Hãy đăng nhập lại");
      this.router.navigate(["/login"]);
    }
  });

  private changePasswordError = catchError((res: HttpErrorResponse) => {
    this.noti.error("Đổi mật khẩu thất bại", res.error);
    throw res;
    return of(null);
  });

  private logoutSuccess = tap(() => {
    const cacheName = this.info.userInfo.fullName;
    this.clearBrowserData();

    this.noti.success(`Tạm biệt ${cacheName}`, "Hẹn gặp lại", {
      nzDuration: 1000,
    });
  });

  private logoutError = catchError((res: HttpErrorResponse) => {
    this.clearInfo();
    this.noti.error("Đăng xuất thất bại", res.message);
    throw res;
    return of(null);
  });

  private clearBrowserData() {
    this.clearInfo();
    this.paymentService.clearDeliveryInfo();
    this.paymentService.clearPaymentMethod();
  }
}
