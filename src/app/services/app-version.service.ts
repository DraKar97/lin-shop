import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

export const VERSION_KEY = "version";

@Injectable({ providedIn: "root" })
export class AppVersionService {
  private readonly API = environment.production
    ? "/api"
    : "http://localhost/api";

  constructor(private http: HttpClient) {}

  get clientVersion() {
    return localStorage[VERSION_KEY];
  }

  set clientVersion(v: string) {
    localStorage[VERSION_KEY] = v;
  }

  getVersion(): Observable<string> {
    return this.http.get(`${this.API}/version`, { responseType: "text" });
  }
}
