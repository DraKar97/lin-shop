import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "vnd",
})
export class VndCurrencyPipe implements PipeTransform {
  transform(value: number, separator = "."): string {
    if (!value) {
      return "";
    }
    const parts = String(value).split(".");
    const str = parts[0]; // trunc
    let result = "";
    for (let i = str.length - 1; i >= 0; i--) {
      const j = str.length - i;
      result = (j % 3 === 0 ? separator : "") + str[i] + result;
    }

    if (str.length % 3 === 0) {
      result = result.slice(1);
    }

    if (parts[1]) {
      result += "," + parts[1];
    }
    result += " vnđ";

    return result;
  }
}
