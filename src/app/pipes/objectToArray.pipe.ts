import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "objToArr",
})
export class ObjectToArrayPipe implements PipeTransform {
  transform(obj: { [key: string]: any }): any[] {
    if (obj == null) return null;

    return Object.keys(obj).map((key) => obj[key]);
  }
}
