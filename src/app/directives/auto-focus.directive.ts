import { AfterViewInit, Directive, ElementRef } from "@angular/core";

@Directive({ selector: "[l-auto-focus]" })
export class AutoFocusDirective implements AfterViewInit {
  constructor(private elRef: ElementRef<HTMLElement>) {}

  ngAfterViewInit() {
    window.requestAnimationFrame(() => {
      this.elRef.nativeElement.focus();
    });
  }
}
