import { Component, OnDestroy } from "@angular/core";
import { Router, ActivationEnd } from "@angular/router";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { Observable, Subscription } from "rxjs";
import { filter, map } from "rxjs/operators";
import { CategoryEnum, getCategoryName } from "./models/category.model";
import { ItemInfo } from "./models/item.model";
import { LoginFormResponse } from "./models/user.model";
import { AppVersionService } from "./services/app-version.service";
import { StoreService } from "./services/store.service";
import { UserService } from "./services/user.service";

const ENABLE_NEWS_KEY = "enable-news";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.less"],
})
export class AppComponent implements OnDestroy {
  title = "lin-shop";

  userLogin: LoginFormResponse;

  clientVersion: string;
  updateVersion$: Subscription;
  totalItems$: Observable<number>;
  activeCategory: { id: string; name: string };
  activeItemName: string;

  get currentUrl(): string {
    return this.router.url;
  }

  get afterLogin() {
    if (this.currentUrl === "/") {
      return {};
    }
    return { nav: this.currentUrl };
  }

  get enableNews(): boolean {
    return localStorage.getItem(ENABLE_NEWS_KEY) === "true";
  }

  set enableNews(v: boolean) {
    localStorage.setItem(ENABLE_NEWS_KEY, String(v));
  }

  private subs: Subscription[];

  constructor(
    private appVersion: AppVersionService,
    private noti: NzNotificationService,
    private user: UserService,
    private router: Router,
    store: StoreService
  ) {
    this.checkVersion(router);

    this.subs = [
      // 1
      this.user.userLogin$.subscribe(
        (userLogin) => (this.userLogin = userLogin)
      ),

      // 2
      router.events
        .pipe(
          filter((e) => e instanceof ActivationEnd),
          map((e: ActivationEnd) => e.snapshot.params.category_id)
        )
        .subscribe(this.onCategorySelected),

      // 3
      store.activeItem$.subscribe(this.onItemSelected),
    ];

    this.totalItems$ = store.cart$.pipe(map(() => store.totalItems));
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }

  onLogout() {
    this.user.logout().subscribe(this.logoutSuccess);
  }

  logoutSuccess = () => {
    const url = this.currentUrl === "/user-info" ? "/" : this.currentUrl;
    this.router.navigate([url]);
  };

  isUpdateWarn = false;
  checkVersion(router: Router) {
    this.updateVersion$ = router.events
      .pipe(filter((e) => e instanceof ActivationEnd))
      .subscribe(this.warnVersionUpdated);
  }

  warnVersionUpdated = () => {
    if (this.isUpdateWarn) {
      this.updateVersion$.unsubscribe();
      return;
    }

    this.clientVersion = this.appVersion.clientVersion;
    if (!this.clientVersion) {
      this.noti.info(
        "Chào mừng bạn lần đầu truy cập Lin-shop",
        "Hãy đặt hàng ủng hộ Lin-shop nhé!"
      );
    }

    this.appVersion.getVersion().subscribe(this.getVersionSuccess);
  };

  getVersionSuccess = (version: string) => {
    if (this.clientVersion !== version) {
      this.appVersion.clientVersion = version;
      if (!this.clientVersion) {
        return;
      }
      this.isUpdateWarn = true;
      this.noti.warning(
        `Lin-shop vừa cập nhật phiên bản mới ${version}`,
        `<div>Bạn đang sử dụng phiên bản ${this.clientVersion}</div><div>hãy "Clear Cache" (Ctrl + F5) để chắc rằng bạn đang sử dụng phiên bản mới nhất</div>`,
        {
          nzDuration: -1,
          nzCloseIcon: "<div></div>",
        }
      );
    }
  };

  onCategorySelected = (category: CategoryEnum) => {
    if (!category) {
      this.activeCategory = null;
      this.activeItemName = null;
      return;
    }
    this.activeCategory = { id: category, name: getCategoryName[category] };
    this.activeItemName = null;
  };

  onItemSelected = (item: ItemInfo) => {
    if (!item) {
      return;
    }
    this.activeCategory = {
      id: item.category,
      name: getCategoryName[item.category],
    };
    this.activeItemName = item.name;
  };
}
