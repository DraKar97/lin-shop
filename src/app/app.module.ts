import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { registerLocaleData } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import en from "@angular/common/locales/en";

// UI Modules
import { HotTableModule } from "@handsontable/angular";
import { NgxMaskModule } from "ngx-mask";
import { NZ_I18N } from "ng-zorro-antd/i18n";
import { en_US } from "ng-zorro-antd/i18n";

import { NzLayoutModule } from "ng-zorro-antd/layout";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzMenuModule } from "ng-zorro-antd/menu";
import { NzBreadCrumbModule } from "ng-zorro-antd/breadcrumb";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzCardModule } from "ng-zorro-antd/card";
import { NzInputNumberModule } from "ng-zorro-antd/input-number";
import { NzMessageModule } from "ng-zorro-antd/message";
import { NzNotificationModule } from "ng-zorro-antd/notification";
import { NzCheckboxModule } from "ng-zorro-antd/checkbox";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzDividerModule } from "ng-zorro-antd/divider";
import { NzAvatarModule } from "ng-zorro-antd/avatar";
import { NzRateModule } from "ng-zorro-antd/rate";
import { NzCommentModule } from "ng-zorro-antd/comment";
import { NzRadioModule } from "ng-zorro-antd/radio";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzSelectModule } from "ng-zorro-antd/select";
import { NzTableModule } from "ng-zorro-antd/table";
import { NzSpinModule } from "ng-zorro-antd/spin";
import { NzModalModule } from "ng-zorro-antd/modal";
import { NzToolTipModule } from "ng-zorro-antd/tooltip";
import { NzBadgeModule } from "ng-zorro-antd/badge";
import { NzDropDownModule } from "ng-zorro-antd/dropdown";
import { NzAlertModule } from "ng-zorro-antd/alert";
import { NzDatePickerModule } from "ng-zorro-antd/date-picker";
import { NzSwitchModule } from 'ng-zorro-antd/switch';

// Pipes
import { VndCurrencyPipe } from "./pipes/vnd-currency.pipe";
import { ObjectToArrayPipe } from "./pipes/objectToArray.pipe";

// Directives
import { AutoFocusDirective } from "./directives/auto-focus.directive";

// Components
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoadingComponent } from "./components/loading/loading.component";
import { SearchComponent } from "./components/search/search.component";
import { ItemCardComponent } from "./components/list-item/item-card/item-card.component";
import { ListItemComponent } from "./components/list-item/list-item.component";
import { RssComponent } from "./components/rss/rss.component";
import { BuyMoreComponent } from "./components/buy-more/buy-more.component";

// Modal
import { RequestCodeModalComponent } from "./components/request-code-modal/request-code-modal.component";
import { ChangePasswordModalComponent } from "./components/change-password-modal/change-password-modal.component";

// View Component
import { HomeComponent } from "./pages/home/home.component.";
import { SignUpComponent } from "./pages/sign-up/sign-up.component";
import { LoginComponent } from "./pages/login/login.component";
import { UserInfoComponent } from "./pages/user-info/user-info.component";
import { CartComponent } from "./pages/cart/cart.component.";
import { CategoryItemComponent } from "./pages/category-item/category-item.component";
import { ItemInfoComponent } from "./pages/item-info/item-info.component";
import { PaymentComponent } from "./pages/payment/payment.component";
import { ThankYouComponent } from "./pages/thank-you/thank-you.component";
import { InviteComponent } from "./pages/invite/invite.component";
import { NotFoundComponent } from './pages/not-found/not-found.component';

import { NZ_CONFIG } from "ng-zorro-antd/core/config";

registerLocaleData(en);
const nzConfig = {
  message: { nzTop: 64 + 12 },
  notification: { nzPlacement: 'bottomRight' },
};

@NgModule({
  declarations: [
    AppComponent,

    // Pipes
    VndCurrencyPipe,
    ObjectToArrayPipe,

    // Directives
    AutoFocusDirective,

    // Components
    LoadingComponent,
    SearchComponent,
    ItemCardComponent,
    ListItemComponent,
    BuyMoreComponent,

    // Modals
    RequestCodeModalComponent,
    ChangePasswordModalComponent,

    // Pages
    HomeComponent,
    SignUpComponent,
    LoginComponent,
    UserInfoComponent,
    CartComponent,
    CategoryItemComponent,
    ListItemComponent,
    ItemInfoComponent,
    PaymentComponent,
    ThankYouComponent,
    NotFoundComponent,

    RssComponent,
    InviteComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,

    // UI Modules
    HotTableModule,
    NgxMaskModule.forRoot(),
    BrowserAnimationsModule,
    NzLayoutModule,
    NzIconModule,
    NzMenuModule,
    NzBreadCrumbModule,
    NzInputModule,
    NzCardModule,
    NzAvatarModule,
    NzRateModule,
    NzCommentModule,
    NzInputNumberModule,
    NzButtonModule,
    NzDividerModule,
    NzMessageModule,
    NzNotificationModule,
    NzCheckboxModule,
    NzRadioModule,
    NzFormModule,
    NzSelectModule,
    NzTableModule,
    NzSpinModule,
    NzModalModule,
    NzToolTipModule,
    NzBadgeModule,
    NzDropDownModule,
    NzAlertModule,
    NzDatePickerModule,
    NzSwitchModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_CONFIG, useValue: nzConfig },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
