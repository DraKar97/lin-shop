import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component.";

import { SignUpComponent } from "./pages/sign-up/sign-up.component";
import { LoginComponent } from "./pages/login/login.component";
import { UserInfoComponent } from './pages/user-info/user-info.component';

import { CartComponent } from "./pages/cart/cart.component.";
import { CategoryItemComponent } from "./pages/category-item/category-item.component";
import { NotFoundComponent } from "./pages/not-found/not-found.component";
import { ItemInfoComponent } from "./pages/item-info/item-info.component";
import { PaymentComponent } from "./pages/payment/payment.component";
import { ThankYouComponent } from "./pages/thank-you/thank-you.component";
import { InviteComponent } from "./pages/invite/invite.component";

const routes: Routes = [
  { path: "", pathMatch: "full", component: HomeComponent },

  { path: "sign-up", component: SignUpComponent },

  { path: "login", component: LoginComponent },
  { path: "user-info", component: UserInfoComponent },

  { path: "categories/:category_id", component: CategoryItemComponent },
  { path: "items", component: CategoryItemComponent },
  { path: "items/:item_id", component: ItemInfoComponent },
  { path: "cart", component: CartComponent },
  { path: "payment", component: PaymentComponent },
  { path: "thank-you/:payment-method", component: ThankYouComponent },

  { path: "invite", component: InviteComponent },

  { path: "404", component: NotFoundComponent },
  { path: "**", redirectTo: "/404" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
