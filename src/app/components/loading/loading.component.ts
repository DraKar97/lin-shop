import { Component, Input } from "@angular/core";

@Component({
  selector: "l-loading",
  templateUrl: "loading.component.html",
  styleUrls: ["loading.component.less"],
})
export class LoadingComponent {
  @Input() loading = false;
}
