import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CategoryEnum } from "src/app/models/category.model";

@Component({
  selector: "l-search",
  templateUrl: "search.component.html",
})
export class SearchComponent {
  searchText = "";
  category: CategoryEnum;

  constructor(private router: Router) {
    //this.category =
  }

  search(v: string) {
    const queryParams = {
      name: v,
      category: this.category,
    };
    this.router.navigate(["/items"], { queryParams });
  }
}
