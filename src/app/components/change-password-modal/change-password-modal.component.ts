import { HttpErrorResponse } from "@angular/common/http";
import { Component } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { errorMonitor } from "events";
import { NzModalRef } from "ng-zorro-antd/modal";
import { ChangePasswordFormModel } from "src/app/models/user.model";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "l-change-password-modal",
  templateUrl: "change-password-modal.component.html",
})
export class ChangePasswordModalComponent {
  validateForm!: FormGroup;

  loading = false;

  get formData(): ChangePasswordFormModel {
    return this.validateForm.getRawValue();
  }

  constructor(
    private fb: FormBuilder,
    private ref: NzModalRef,
    private user: UserService
  ) {}

  submitForm(): void {
    if (this.validateForm.valid) {
      this.loading = true;
      this.validateForm.disable();
      this.changePassword();
      return;
    }

    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() =>
      this.validateForm.controls.checkPassword.updateValueAndValidity()
    );
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      oldPassword: [null, [Validators.required]],
      password: [null, [Validators.required]],
      checkPassword: [null, [Validators.required, this.confirmationValidator]],
    });
  }

  changePassword() {
    let data = this.formData;
    delete data.checkPassword;
    this.user.changePassword(data).subscribe();
  }

  onCancel() {
    this.ref.destroy();
  }
}
