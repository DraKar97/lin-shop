import { Component, ElementRef, Input, OnInit } from "@angular/core";
import { RssPage } from "src/app/models/rss-page.model";
import { RssService } from "src/app/services/rss.service";

@Component({
  selector: "l-rss",
  template: "",
  styleUrls: ["./rss.component.less"],
})
export class RssComponent implements OnInit {
  @Input() page: RssPage;

  constructor(
    private service: RssService,
    private elRef: ElementRef<HTMLElement>
  ) {}

  ngOnInit() {
    this.service.getRss(this.page).subscribe((xml) => {
      this.elRef.nativeElement.innerHTML = xml;

      this.service.parseRss(this.page).subscribe((json) => {
        this.elRef.nativeElement.innerHTML = "";
        json.forEach((item) => {
          let titleEl = document.createElement("div");
          titleEl.innerText = `${item.title} (${item.pubDate})`;
          titleEl.className = "title";
          let contentEl = document.createElement("div");
          contentEl.innerHTML = item.description;
          let itemEl = document.createElement("div");
          itemEl.appendChild(titleEl);
          itemEl.appendChild(contentEl);
          this.elRef.nativeElement.appendChild(itemEl);
        });
      });
    });
  }
}
