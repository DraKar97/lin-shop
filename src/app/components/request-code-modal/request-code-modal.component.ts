import { Component, OnInit } from "@angular/core";
import { NzModalRef } from "ng-zorro-antd/modal";
import { NzNotificationService } from "ng-zorro-antd/notification";
import { RequestCodeResponse } from "src/app/models/mailchimp.model";
import { MailchimpService } from "src/app/services/mailchimp.service";

@Component({
  selector: "l-request-code-modal",
  templateUrl: "request-code-modal.component.html",
})
export class RequestCodeModalComponent {
  requestCodeDone = false;
  passwordVisible = false;
  password = "";

  constructor(
    private ref: NzModalRef,
    private mailchimp: MailchimpService,
    private noti: NzNotificationService
  ) {}

  onRequestCode() {
    this.mailchimp.requestCode("aido").subscribe(this.requestCodeResponse);
  }

  onConfirm() {
    this.ref.close(this.password);
  }

  onCancel() {
    this.ref.close();
  }

  requestCodeResponse = ({ error, status }: RequestCodeResponse) => {
    if (error) {
      this.noti.error("Yêu cầu mã xác thực thất bại", error);
      return;
    }
    if (status) {
      this.requestCodeDone = true;
      return;
    }
  };
}
