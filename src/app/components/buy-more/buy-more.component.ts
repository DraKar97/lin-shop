import { Component } from "@angular/core";

@Component({
  selector: "l-buy-more",
  templateUrl: "buy-more.component.html",
  styles: [
    `
      #banner {
        height: calc(100% - 44px - 14px - 24px);
        width: 100%;
      }
    `,
  ],
})
export class BuyMoreComponent {}
