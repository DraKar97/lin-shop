import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { NzMessageService } from "ng-zorro-antd/message";
import { ItemInfo } from "src/app/models/item.model";
import { StoreService } from "src/app/services/store.service";

@Component({
  selector: "l-item-card",
  templateUrl: "item-card.component.html",
  styleUrls: ["item-card.component.less"],
})
export class ItemCardComponent {
  @Input() item: ItemInfo;
  @Input() link: string;
  @Input() showActions: boolean = true;

  constructor(
    private router: Router,
    private store: StoreService,
    private messageService: NzMessageService
  ) {}

  get message(): string {
    return `1 "${this.item.name}" đã được thêm vào giỏ`;
  }

  onAddOnce() {
    this.store.addToCart(this.item, 1);
    this.messageService.success(this.message);
  }

  onAddThenNavToCart() {
    this.onAddOnce();
    this.router.navigate(["/cart"]);
  }
}
