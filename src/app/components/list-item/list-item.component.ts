import { Component, Input } from "@angular/core";
import {
  CategoryEnum,
  getDefaultItemImage,
} from "src/app/models/category.model";
import { ItemModel } from "src/app/models/item.model";

@Component({
  selector: "l-list-item",
  templateUrl: "list-item.component.html",
  styleUrls: ["list-item.component.less"],
})
export class ListItemComponent {
  @Input() category: CategoryEnum;
  @Input() set items(v: ItemModel[]) {
    this._items = this.setItems(v);
  }

  _items: ItemModel[];

  setItems(v: ItemModel[]): ItemModel[] {
    return (
      v ||
      [].map((item) => ({
        ...item,
        src: item.src || getDefaultItemImage[this.category],
      }))
    );
  }
}
