export enum ResponseCode {
  OK = 200,
  BAD_REQUEST = 400,
  UNAUTHORIZE = 401,
}
