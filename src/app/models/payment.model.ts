export enum PAYMENT_METHOD {
  ATM = "atm",
  COD = "cod",
  PAYPAL = "paypal",
  NGAN_LUONG = "ngan-luong",
}
