export interface ItemModel {
  id: string;
  name: string;
  price: number;
  /** url of image */
  src?: string;
}

export interface ItemInfo extends Partial<ItemModel> {
  category: string;
  description?: string;
  rate?: 0 | 1 | 2 | 3 | 4 | 5;
  comments?: {
    [userId: string]: {
      message: string;
      comments?: { [userId: string]: string }[];
    };
  };
}
