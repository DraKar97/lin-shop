export interface UserInfo {
  email: string;
  fullName: string;
  phoneNumber: string;
  phoneNumberPrefix: string;
  address: string;
  cmnd: string;
  dob: string;
}

export interface SignupFormModel extends Partial<UserInfo> {
  password: string;
  checkPassword: string;
  agree: boolean;
}

export interface LoginFormModel {
  userName: string;
  password: string;
  remember: boolean;
}

export interface LoginFormResponse {
  access_token: string;
  userInfo: {
    email: string;
    fullName: string;
  };
}

export interface ChangePasswordFormModel {
  oldPassword: string;
  password: string;
  checkPassword: string;
}
