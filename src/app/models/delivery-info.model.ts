export interface DeliveryInfo {
  email?: string;
  name?: string;
  phonePrefix: string;
  phone: string;
  address: string;
}
