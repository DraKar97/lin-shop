export const TOTAL_CATEGORY = 4;
export enum CategoryEnum {
  CAFE = "cafe",
  TRA_SUA = "tra-sua",
  NUOC_EP = "nuoc-ep",
  NUOC_NGOT = "nuoc-ngot",
}
export const CATEGORIES: string[] = [
  CategoryEnum.CAFE,
  CategoryEnum.TRA_SUA,
  CategoryEnum.NUOC_EP,
  CategoryEnum.NUOC_NGOT,
];
export const getCategoryName = {
  [CategoryEnum.CAFE]: "Cafe",
  [CategoryEnum.TRA_SUA]: "Trà sữa",
  [CategoryEnum.NUOC_EP]: "Nước ép",
  [CategoryEnum.NUOC_NGOT]: "Nước ngọt",
};
export const getDefaultItemImage = {
  [CategoryEnum.CAFE]: "/assets/cafe.jpg",
  [CategoryEnum.TRA_SUA]: "/assets/tra-sua.jpg",
  [CategoryEnum.NUOC_EP]: "/assets/nuoc-ep.jpg",
  [CategoryEnum.NUOC_NGOT]: "/assets/nuoc-ngot.jpg",
};

// Fake data
// const CAFE_DATA: ItemModel[] = [
//   {
//     id: "cafe-sua-nong",
//     name: "Cafe sữa nóng",
//     price: 20000,
//     src: "/assets/cafe-sua-nong.jpg",
//   },
//   {
//     id: "cafe-sua-da",
//     name: "Cafe sữa đá",
//     price: 22000,
//     src: "/assets/cafe-sua-da.jpg",
//   },
//   {
//     id: "cafe-da-lon",
//     name: "Cafe đá lớn",
//     price: 15000,
//     src: "/assets/cafe-da.jpg",
//   },
//   {
//     id: "cafe-da-nho",
//     name: "Cafe đá nhỏ",
//     price: 12000,
//     src: "/assets/cafe-da.jpg",
//   },
//   {
//     id: "cafe-nong-lon",
//     name: "Cafe nóng lớn",
//     price: 14000,
//     src: "/assets/cafe-nong.jpg",
//   },
//   {
//     id: "cafe-nong-nho",
//     name: "Cafe nóng nhỏ",
//     price: 11000,
//     src: "/assets/cafe-nong.jpg",
//   },
// ];

// const TRA_SUA_DATA: ItemModel[] = [
//   {
//     id: "tra-sua-nho",
//     name: "Trà sữa nhỏ",
//     price: 15000,
//     src: "/assets/tra-sua-truyen-thong.jpg",
//   },
//   {
//     id: "tra-sua-lon",
//     name: "Trà sữa lớn",
//     price: 20000,
//     src: "/assets/tra-sua-truyen-thong.jpg",
//   },
//   {
//     id: "tra-sua-tran-chau-duong-den",
//     name: "Trà sữa trân châu đường đen",
//     price: 25000,
//     src: "/assets/tra-sua-tran-chau-duong-den.jpg",
//   },
//   {
//     id: "tra-sua-tran-chau-nho",
//     name: "Trà sữa trân châu nhỏ",
//     price: 20000,
//   },
//   {
//     id: "tra-sua-tran-chau-lon",
//     name: "Trà sữa trân châu lớn",
//     price: 25000,
//   },
//   {
//     id: "tra-sua-matcha-nho",
//     name: "Trà sữa matcha nhỏ",
//     price: 18000,
//     src: "/assets/tra-sua-matcha.jpg",
//   },
//   {
//     id: "tra-sua-matcha-lon",
//     name: "Trà sữa matcha lớn",
//     price: 23000,
//     src: "/assets/tra-sua-matcha.jpg",
//   },
// ];

// const NUOC_EP_DATA: ItemModel[] = [
//   {
//     id: "nuoc-cam-ep-nho",
//     name: "Nước cam ép nhỏ",
//     price: 25000,
//     src: "/assets/nuoc-ep-cam.jpg",
//   },
//   {
//     id: "nuoc-cam-ep-lon",
//     name: "Nước cam ép lớn",
//     price: 30000,
//     src: "/assets/nuoc-ep-cam.jpg",
//   },
//   {
//     id: "nuoc-buoi-ep-nho",
//     name: "Nước bưởi ép nhỏ",
//     price: 24000,
//     src: "/assets/nuoc-ep-buoi.jpg",
//   },
//   {
//     id: "nuoc-buoi-ep-lon",
//     name: "Nước bưởi ép lớn",
//     price: 29000,
//     src: "/assets/nuoc-ep-buoi.jpg",
//   },
//   {
//     id: "nuoc-dau-ep-nho",
//     name: "Nước dâu ép nhỏ",
//     price: 25000,
//     src: "/assets/nuoc-ep-dau.jpg",
//   },
//   {
//     id: "nuoc-dau-ep-lon",
//     name: "Nước dâu ép lớn",
//     price: 30000,
//     src: "/assets/nuoc-ep-dau.jpg",
//   },
//   {
//     id: "nuoc-ep-carot",
//     name: "Nước ép carot",
//     price: 20000,
//     src: "/assets/nuoc-ep-carot.jpg",
//   },
// ];

// const NUOC_NGOT_DATA: ItemModel[] = [
//   {
//     id: "pepsi",
//     name: "Pepsi",
//     price: 12000,
//   },
//   {
//     id: "coca",
//     name: "Coca",
//     price: 12000,
//     src: "/assets/coca.jpg",
//   },
//   {
//     id: "sting-dau",
//     name: "Sting dâu",
//     price: 10000,
//     src: "/assets/sting-dau.jpg",
//   },
//   {
//     id: "sting-gold",
//     name: "Sting gold",
//     price: 10000,
//     src: "/assets/sting-gold.jpg",
//   },
//   {
//     id: "7up",
//     name: "7 Up",
//     price: 75000,
//     src: "/assets/7up.jpg",
//   },
//   {
//     id: "o-long",
//     name: "Ô long",
//     price: 12000,
//     src: "/assets/o-long.jpg",
//   },
//   {
//     id: "tra-xanh-0-do",
//     name: "Trà xanh 0 độ",
//     price: 12000,
//     src: "/assets/tra-xanh-0-do.jpg",
//   },
// ];

// export const DATA = {
//   [CategoryEnum.CAFE]: CAFE_DATA,
//   [CategoryEnum.TRA_SUA]: TRA_SUA_DATA,
//   [CategoryEnum.NUOC_EP]: NUOC_EP_DATA,
//   [CategoryEnum.NUOC_NGOT]: NUOC_NGOT_DATA,
// };
