export interface PaypalGetTokenResponse {
  scope: string;
  access_token: string;
  token_type: "Bearer";
  app_id: string;
  expires_in: number;
  nonce: string;
}

export interface PaypalCheckoutResponse {
  id: string;
  intent: string;
  status: string;
  purchase_units: {
    reference_id: string;
    amount: { currency_code: string; value: string };
    payee: { email_address: string; merchant_id: string };
  }[];
  create_time: string; //datetime
  /** [self,approve,update,capture] */
  links: { href: string; rel: string; method: string }[];
}
