import { Router } from "@angular/router";

export const ignoreError = () => {};

export const getNavTarget = (router: Router) => {
  const url = router.url;
  const urlTree = router.parseUrl(url);
  return urlTree.queryParams.nav;
};
