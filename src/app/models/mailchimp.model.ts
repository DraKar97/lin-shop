export interface MailchimpUserInfo {
  email_address: string;
  merge_fields?: {
    LNAME?: string;
    FNAME?: string;
    BIRTHDAY?: string;
    PHONE?: string;
  };
  status?: MailchimpUserStatus;
  id?: string;
}

export const MailchimpUserInfoSchema: MailchimpUserInfo = {
  id: null,
  email_address: null,
  merge_fields: { FNAME: null, LNAME: null, PHONE: null, BIRTHDAY: null },
  status: null,
};

export interface SendAllResponse {
  isStart: boolean;
  error: string;
}

export interface RequestCodeResponse {
  status?: "queued";
  error?: string;
}

export enum MailchimpUserStatus {
  SUBSCRIBED = "subscribed",
  UNSUBSCRIBED = "unsubscribed",
  PENDING = "pending",
  CLEANED = "cleaned",
  /** To add */
  TRANSACTIONAL = "transactional",
  ERROR = 400,
}
