export enum RssPage {
  VNEXPRESS = "vnexpress",
  _24H = "24h",
}

export interface RssItem {
  title: string;
  /** html element */
  description: string;
  /** date string */
  pubDate: string;
}

export type RssModel = RssItem[];
