import { ItemInfo } from "./item.model";

export const CART_KEY = "cart";
export type PurchaseItem = ItemInfo & { quantity: number };
export type Cart = { [itemId: string]: PurchaseItem };
export type SimpleCart = { id: string; quantity: number }[];
export type BillInfo = {
  totalItems: number;
  totalValue: number;
  fees: {
    [key: string]: { value: number; description: string };
    nganluong?: { value: number; description: string };
  };
  discounts: { [key: string]: { value: number; description: string } };
  summary: number;
};
