const db = require("./_firebase");
const paypal = require("./_paypal");
const nganluong = require("./_ngan-luong");
const user = require("./_user");
const utils = require("./_utils");

const DEFAULT_SHIPPING_FEE = 30000; // vnd

module.exports = {
  config,
  getVersion,
  setVersion,

  handleAppListen,
  getItemsByCategory,
  getItemsByName,
  getItemById,

  // payment
  handleGetBillInfo,

  handlePayCodCheckout,
  handlePayCodCapture,

  handlePaypalCheckout_v2,
  handlePaypalHook,
  handlePaypalCapture,

  handleNganLuongCheckout_v3,
  handleNganLuongCapture,

  // files
  handleDefaultTemplate,
  sendRobotsTxt,
  sendSitemapXml,
};

var html;
var robotsTxt;
var host;
var version;

/** Clone */
const PAYMENT_METHOD = {
  COD: "COD",
  PAYPAL: "paypal",
  NGAN_LUONG: "ngan-luong",
};

function config(v) {
  host = v.host;

  html = v.root + "/index.html";
  robotsTxt = v.root + "/assets/robots.txt";
  sitemapXml = v.root + "/assets/sitemap.xml";

  paypal.config({ host, isWriteLog: v.isWriteLog });
  nganluong.config({ host });
  return this;
}

function setVersion(v) {
  version = `${v}`;
}

function getVersion(req, res) {
  res.send(`v${version}`);
}

function handleAppListen(port) {
  console.log(`Lin-shop v${version} listenning port: ${port}`);
}

const seo = require("./_seo");
async function handleDefaultTemplate(req, res) {
  console.log(`[${req.method}]`, `${req.headers.host}${req.url}`);

  const paths = req.url.split("/");
  let fileContent;
  if (paths[1] === "items") {
    const itemInfo = await db.getItemById(paths[2]);
    fileContent = await seo.appendMeta(html, paths[2], itemInfo);
  } else {
    fileContent = await seo.appendMeta(html);
  }

  res.send(fileContent);
}

function sendRobotsTxt(req, res) {
  res.sendFile(robotsTxt);
}

function sendSitemapXml(req, res) {
  res.sendFile(sitemapXml);
}

async function getItemsByCategory(req, res) {
  const { category } = req.params;
  const items = await db.getItemsByCategory(category);

  res.send(items);
}

async function getItemsByName(req, res) {
  const { name, category } = req.query;
  const items = await db.getItemsByName(name, category);
  res.send(items);
}

async function getItemById(req, res) {
  const { id } = req.params;
  const item = await db.getItemById(id);

  if (!item) {
    res.status(404).end();
    return;
  }

  res.send(item);
}

async function handleGetBillInfo(req, res) {
  const { cart } = req.body;
  let billInfo = await getDefaultBillInfo(cart);

  if (await user.isAuthorize(req)) {
    const { email } = req.body;
    await appenMemberDiscount(billInfo, email);
    res.send(billInfo);
    return;
  }

  res.send(billInfo);
}

async function getDefaultBillInfo(cart) {
  const { total: totalValue } = await utils.buildPaypalOrder(cart, 1, "VND");
  const shippingFee = DEFAULT_SHIPPING_FEE;
  const summary = totalValue + shippingFee;
  const billInfo = {
    totalValue,
    fees: {
      shipping: { value: shippingFee, description: "Giao hàng" },
    },
    summary,
  };

  return billInfo;
}

async function appenMemberDiscount(billInfo, email) {
  let { summary } = billInfo;

  const user = await db.getUserByEmail(email);
  const discountPercent =
    Object.keys(user).filter((key) => user[key].length && user[key].length > 0)
      .length * 2;
  const memberDiscount = utils.parseToTwoDecimals(
    (discountPercent * summary) / 100
  );

  summary -= memberDiscount;
  Object.assign(billInfo, {
    discounts: {
      member: {
        value: memberDiscount,
        description: `Thành viên -${discountPercent}%`,
      },
    },
    summary,
  });
}

async function handlePayCodCheckout(req, res) {
  const { cart, buyerInfo } = req.body;
  const billInfo = await getDefaultBillInfo(cart);
  if (await user.isAuthorize(req)) {
    const { email } = req.body;
    await appenMemberDiscount(billInfo, email);
  }

  const token = new Date().valueOf();
  await db.addBill(
    PAYMENT_METHOD.COD,
    token,
    cart,
    billInfo,
    buyerInfo,
    "CREATED"
  );

  const approveUrl = `${host}/thank-you/cod?token=${token}`;

  res.end(approveUrl);
}

async function handlePayCodCapture(req, res) {
  const { token } = req.body;

  const bill = await db.updateBill(token, "COMPLETED");
  console.log("--- take money =", bill);
  res.send({ status: "COMPLETED" });
}

/**
 * Listen webhook Checkout order approved (without verifyed)
 * @warn Out of date
 * @see https://developer.paypal.com/docs/api/webhooks/v1/#verify-webhook-signature
 */
async function handlePaypalHook(req, res) {
  const { event_type, resource } = req.body;
  const { id } = resource;
  console.log("--- event =", event_type, `[${id}]`);

  // resolve problem ngrox or paypal event fire duplicate
  if (
    event_type === "CHECKOUT.ORDER.APPROVED" &&
    resource.status &&
    resource.status === "APPROVED"
  ) {
    // ignore Authorize buyer funds
    const res = await paypal.captureOrder_v2(id);
    const { message, status, purchase_units } = res;
    console.log("--- take money =", message || status);

    if (purchase_units) {
      paypal.utils.logTranferMoney(purchase_units);
    }
  } else {
    console.error("--- reject capture invoice =", id);
  }
}

async function handlePaypalCheckout_v2(req, res) {
  const { cart, buyerInfo } = req.body;
  const billInfo = await getDefaultBillInfo(cart);
  if (await user.isAuthorize(req)) {
    const { email } = req.body;
    await appenMemberDiscount(billInfo, email);
  }

  const access_token = await paypal.getToken();
  console.log("--- access_token =", access_token);

  const checkoutRes = await paypal.createOrder_v2(cart, billInfo, buyerInfo);
  if (!checkoutRes.links[1]) {
    res.status(400).end();
    return;
  }
  console.log("--- checkout =", checkoutRes.links[1].href);

  db.addBill(
    PAYMENT_METHOD.PAYPAL,
    checkoutRes.id,
    cart,
    billInfo,
    buyerInfo,
    checkoutRes.status
  );

  // to send UI approve URL
  res.end(checkoutRes.links[1].href);
}

async function handlePaypalCapture(req, res) {
  const { token, PayerID } = req.body;

  if (!token || !PayerID) return;

  const captureRes = await paypal.captureOrder_v2(token);
  const { id, message, status, purchase_units } = captureRes;

  db.updateBill(id, status);
  console.log("--- take money =", message || status);
  res.send({ status });

  if (purchase_units) {
    paypal.utils.logTranferMoney(purchase_units);
  }
}

async function handleNganLuongCheckout_v3(req, res) {
  const { cart, buyerInfo } = req.body;

  const billInfo = await getDefaultBillInfo(cart);
  if (await user.isAuthorize(req)) {
    const { email } = req.body;
    await appenMemberDiscount(billInfo, email);
  }

  const checkoutRes = await nganluong.createOrder_v3(cart, billInfo, buyerInfo);
  console.log(
    "--- checkout =",
    checkoutRes.checkout_url ||
      checkoutRes.error_code + " " + checkoutRes.description
  );

  // No error
  if (checkoutRes.error_code === "00") {
    db.addBill(
      PAYMENT_METHOD.NGAN_LUONG,
      checkoutRes.token,
      cart,
      billInfo,
      buyerInfo,
      "CREATED"
    );

    // to send UI approve URL
    res.end(checkoutRes.checkout_url);
  }

  res.status(500).send({ error: checkoutRes.description });
}

async function handleNganLuongCapture(req, res) {
  const { token } = req.body;
  const captureRes = await nganluong.captureOrder_v3(token);
  console.log(
    "--- take money =",
    captureRes.transaction_status === "00" ? captureRes.total_amount : "ERROR"
  );

  if (captureRes.transaction_status === "00") {
    await db.updateBill(token, "COMPLETED");
    res.send({ status: "COMPLETED" });
  } else {
    res.send({ status: "ERROR" });
  }
}
