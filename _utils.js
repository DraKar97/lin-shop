const db = require("./_firebase");
const xmlParser = require("xml2js");
const fs = require("fs");

module.exports = {
  buildPaypalOrder,
  logTranferMoney,
  parseToTwoDecimals,
  parseXmlToJson,
  readFile,
  sleep,
};

async function buildPaypalOrder(cart, exchange, currency_code) {
  let items = [];
  let total = 0;

  for (let o of cart) {
    const { id, quantity } = o;
    const _item = await db.getItemById(id);
    const item = {
      name: id, //_item.name, // Not support Tieng Viet
      quantity,
      unit_amount: {
        value: parseToTwoDecimals(_item.price / exchange), // only allow 2 decimals
        currency_code,
      },
    };
    total += parseToTwoDecimals(item.unit_amount.value * item.quantity);
    items.push(item);
  }
  total = parseToTwoDecimals(total);

  return { items, total };
}

function logTranferMoney(items) {
  let total = {};
  for (let i = 0; i < items.length; i++) {
    const captures = items[i].payments && items[i].payments.captures;
    if (captures && captures.length) {
      for (let j = 0; j < captures.length; j++) {
        console.log(captures[j].amount);
        const { currency_code, value } = captures[j].amount;
        if (total[currency_code]) {
          total[currency_code] += parseFloat(value);
        } else {
          total[currency_code] = parseFloat(value);
        }
      }
    }
  }
  console.log("__________________________________________");
  for (let currency in total) {
    console.log(total[currency], currency);
  }
}

function parseToTwoDecimals(num) {
  return Math.round(100 * num) / 100;
}

async function parseXmlToJson(xmlContent) {
  return await xmlParser.parseStringPromise(xmlContent);
}

async function readFile(filePath) {
  if (!filePath) {
    return null;
  }
  const res = new Promise((_) => {
    fs.readFile(filePath, (err, content) => {
      if (err) {
        console.error("--- read file error =", err);
        return;
      }
      _(content.toString());
    });
  });
  return await res;
}

async function sleep(ms) {
  return await new Promise((_) => {
    setTimeout(() => {
      _(ms);
    }, ms);
  });
}
