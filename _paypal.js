var host;
var token;

const http = require("./_request");
const db = require("./_firebase");
const utils = require("./_utils");

module.exports = {
  getToken,
  createOrder_v2,
  captureOrder_v2,
  config,
  utils,
};

// ================================== PAYPAL INFOMATION ==================================

const client_id =
  "AbSMgZk118FXKkKPZfSL19cVYO5J06B7AebNeFkzIz0G2K-l05rpZUcTOMNYSemKKWgjbK5N8t3BcU1M";
const client_secret =
  "EFvC8G6uSd3DPFvKR8MlVb3E3MRDi6H6GGkcub43g4UBzTak5DmIXYp4upMcRHZVmSnStNGXBK4tDePs";
const auth_str = client_id + ":" + client_secret;
// ================================== PAYPAL INFOMATION ==================================

const HOST_PAYPAL = "api.sandbox.paypal.com";
const URL_ORDER = "/v2/checkout/orders";
const URL_TOKEN = "/v1/oauth2/token";
const REQUEST_TOKEN_DATA = "grant_type=client_credentials";
const REQUEST_TOKEN_AUTH = "Basic " + Buffer.from(auth_str).toString("base64");
const URL_CAPTURE = (id) => URL_ORDER + "/" + id + "/capture";
const currency_code = "USD";

async function getToken() {
  const { access_token } = await http.request(
    HOST_PAYPAL,
    URL_TOKEN,
    REQUEST_TOKEN_AUTH,
    REQUEST_TOKEN_DATA
  );
  token = access_token;
  return access_token;
}

/** @see https://developer.paypal.com/docs/api/orders/v2/ */
async function createOrder_v2(cart, billInfo, buyerInfo) {
  console.log("--- cart =", cart);
  console.log("--- billInfo =", billInfo);
  const USD_Exchange = await db.getExchange(currency_code);
  const shipping = utils.parseToTwoDecimals(
    billInfo.fees.shipping.value / USD_Exchange
  ); // only allow 2 decimals
  const summary = utils.parseToTwoDecimals(billInfo.summary / USD_Exchange);

  let discount;
  if (billInfo.discounts) {
    discount = utils.parseToTwoDecimals(
      billInfo.discounts.member.value / USD_Exchange
    );
  }
  const { items, total } = await utils.buildPaypalOrder(
    cart,
    USD_Exchange,
    currency_code
  );

  let payer;
  if (buyerInfo.name || buyerInfo.email) {
    payer = {};
    if (buyerInfo.name) {
      payer.name = { given_name: buyerInfo.name };
    }
    if (buyerInfo.email) {
      payer.email_address = buyerInfo.email;
    }
    // phone: {
    //   national_number: buyerInfo.nationalCode || '+84',
    //   phone_number: `${buyerInfo.phone}`,
    // },
    // address: { address_line_1: buyerInfo.address },
  }

  let billData = {
    intent: "CAPTURE",
    purchase_units: [
      {
        //reference_id: "default",
        amount: {
          value: summary,
          currency_code,
          // @see https://developer.paypal.com/docs/api/orders/v2/#definition-amount_with_breakdown
          breakdown: {
            item_total: { value: total, currency_code },
            shipping: { value: shipping, currency_code },
          },
        },
        items,
      },
    ],
    application_context: {
      // landing_page: "BILLING", //NO_PREFERENCE|LOGIN|BILLING
      user_action: "PAY_NOW", // || CONTINUE
      // payment_method: {
      //   payer_selected: "PAYPAL", // CARD??
      // },
      return_url: `${host}/thank-you/paypal`,
      cancel_url: `${host}/payment`,
    },
  };
  if (payer) {
    billData.payer = payer;
  }
  if (discount) {
    billData.purchase_units[0].amount.breakdown.discount = {
      value: discount,
      currency_code,
    };
  }

  console.log("billData", billData.purchase_units[0]);
  return await http.post(
    HOST_PAYPAL,
    URL_ORDER,
    auth(),
    JSON.stringify(billData)
  );
}

async function captureOrder_v2(id) {
  return await http.post(HOST_PAYPAL, URL_CAPTURE(id), auth(), null);
}

function config(v) {
  host = v.host;
  if (v.isWriteLog) {
    http.enableLog();
  }
  return this;
}

auth = () => "Bearer " + token;
