const http = require("./_request");
const utils = require("./_utils");

module.exports = {
  getVnexpress,
  parseVnexpress,

  get24h,
  parse24h,
};

let cache = { vnexpress: "", _24h: "" };

const vnexpress = { host: "vnexpress.net", url: "/rss/tin-xem-nhieu.rss" };
const _24h = { host: "cdn.24h.com.vn", url: "/upload/rss/amthuc.rss" }; // TODO: get 24h rss

// vnexpress.net
async function getVnexpress(req, res) {
  const rssContent = await getRssByUrl(vnexpress.host, vnexpress.url);
  cache.vnexpress = rssContent;
  res.send(rssContent);
}

async function parseVnexpress(req, res) {
  if (cache.vnexpress) {
    const jsonContent = await parseRssContent(cache.vnexpress);
    res.send(jsonContent);
  } else {
    res.send(new Error("Please wait get RSS first"));
  }
}

// 24h.com.vn
async function get24h(req, res) {
  const rssContent = await getRssByUrl(_24h.host, _24h.url);
  cache._24h = rssContent;
  res.send(rssContent);
}

async function parse24h(req, res) {
  if (cache._24h) {
    const jsonContent = await parseRssContent(cache._24h);
    res.send(jsonContent);
  } else {
    res.send(new Error("Please wait get RSS first"));
  }
}

async function getRssByUrl(host, url) {
  return await http.get(host, url, null, "");
}

async function parseRssContent(xmlContent) {
  const jsonContent = await utils.parseXmlToJson(xmlContent);
  const content = jsonContent.rss.channel[0].item.map((o) => {
    let obj = {};
    Object.keys(o).forEach((key) => {
      obj[key] = o[key][0];
    });
    return obj;
  });
  return content;
}
