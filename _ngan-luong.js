var host;

/**
9874563254178962
Nguyen Hue
123456
*/

const merchant_id = "49554";
const SHOP_EMAIL = "nguoiban.lin-shop@yopmail.com";
const merchant_password_org = "3ad0645d031e95c3aa37b655124dffe7";
const LIFE_TIME = 5; // mins
const PAYMENT_METHOD = { ATM_ONLINE: "ATM_ONLINE", NL: "NL" };

const API_VERSION = 3.1;
const URL_ORDER =
  "https://sandbox.nganluong.vn:8088/nl35/checkout.api.nganluong.post.php";
const URL_CAPTURE = URL_ORDER; //"https://sandbox.nganluong.vn:8088/nl35/service/order/check";
const FUNCTION_ORDER = "SetExpressCheckout";
const FUNCTION_CAPTURE = "GetTransactionDetail";

const md5 = require("./_md5");
const http = require("request"); //require("./_request");
const db = require("./_firebase");
const utils = require("./_utils");

module.exports = {
  config,

  createOrder_v3,
  captureOrder_v3,
};

const merchant_password = md5(merchant_password_org);
const currency_code = "VND";

async function createOrder_v3(
  cart,
  billInfo,
  buyer,
  order_code = "day_la_cai_gi"
) {
  console.log("--- cart =", cart[0], " ...");
  console.log("--- buyer =", buyer);

  const exchange = await db.getExchange(currency_code);
  const shipping = utils.parseToTwoDecimals(
    billInfo.fees.shipping.value / exchange
  );
  let discount;
  if (billInfo.discounts) {
    discount = utils.parseToTwoDecimals(
      billInfo.discounts.member.value / exchange
    );
  }
  const { items, total } = await utils.buildPaypalOrder(
    cart,
    exchange,
    currency_code
  );

  const billData = {
    merchant_id,
    merchant_password,
    version: API_VERSION,
    function: FUNCTION_ORDER,
    time_limit: LIFE_TIME,
    payment_type: "1", // no delay PAYMEN_METHOD.NL

    receiver_email: SHOP_EMAIL,
    order_code,
    return_url: `${host}/thank-you/ngan-luong`,
    cancel_url: `${host}/payment`,

    payment_method: PAYMENT_METHOD.ATM_ONLINE,
    bank_code: "EXB",
    buyer_email: buyer.email, // required
    buyer_mobile: buyer.phone,
    buyer_address: buyer.address,

    cur_code: currency_code.toLowerCase(),
    total_amount: total,
    fee_shipping: shipping,
  };

  if (buyer.name) {
    billData.buyer_fullname = buyer.name;
  }

  billData.total_item = cart.length;
  // NganLuong support only 1 item, @see code example, integreate document
  // item_name${i} not effect??
  items.forEach((item, i) => {
    billData[`item_name${i + 1}`] = item.name;
    billData[`item_quantity${i + 1}`] = item.quantity;
    billData[`item_amount${i + 1}`] = item.unit_amount.value;
    billData[`item_url${i + 1}`] = item.url || "http://localhost:4200"; // TODO: update url
  });
  billData.order_description = items
    .map((item) => `${item.name}: ${item.quantity} x ${item.unit_amount.value}`)
    .reduce((a, b) => a + b);

  if (discount) {
    billData.discount_amount = discount;
  }

  var options = {
    method: "POST",
    url: URL_ORDER,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    form: billData,
  };
  const res = await request(options);
  return utils.parseXmlToJson(res).then(reduceResponse);
}

async function captureOrder_v3(token) {
  const options = {
    method: "POST",
    url: URL_CAPTURE,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    form: {
      merchant_id,
      merchant_password,
      token,
      version: API_VERSION,
      function: FUNCTION_CAPTURE,
    },
  };
  const res = await request(options);
  return utils.parseXmlToJson(res).then(reduceResponse);
}

function request(options) {
  return new Promise((_) =>
    http(options, (err, res) => {
      if (err) {
        throw new Error(err);
      }
      _(res.body);
    })
  );
}

function reduceResponse({ result }) {
  let obj = {};
  Object.keys(result).forEach((key) => {
    obj[key] = result[key][0];
  });
  return obj;
}

function config(v) {
  host = v.host;
  if (v.isWriteLog) {
    http.enableLog();
  }
  return this;
}
