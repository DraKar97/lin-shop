const request = require("request");

module.exports = {
  handleFbAddHook,
  handleFbListenHook,
};

const FB_VERIFY_CODE = "linhcode";
const pageToken =
  "EAAKfFwHQtC0BAPQayAzWWrZA3HOj4VMNIjZCMZBZAi3ZAMTmVpAsxw2lQRmCbWZBNZBgz69O0c14fp4xLKYO7sn0vlMURyD3pn1qt7UYj1mqWNrumoNLT8e6BwVMaQno1cuZApWSpBnoQYZBZAqoPeuPQZBvFwPFxvxrSpfmtj2cqgYKAZDZD";

function handleFbAddHook(req, res) {
  let mode = req.query["hub.mode"];
  let token = req.query["hub.verify_token"];
  let challenge = req.query["hub.challenge"];
  console.log("--- reg fbhook =", req.query, req.body);

  if (mode && token) {
    if (mode === "subscribe" && token === FB_VERIFY_CODE) {
      res.status(200).send(challenge);
      console.log("--- fb hook =", challenge);
    } else {
      res.sendStatus(403);
      console.log("--- fb hook = ERROR");
    }
  }
}

// NOTE: Can't receive when using mobile platform
async function replyFbMessenger(recipient, text) {
  console.log({
    // messaging_type: "",
    recipient, // TODO: verify model
    message: {
      text,
    },
  });
  const body = {
    // messaging_type: "",
    recipient,
    message: {
      text,
    },
  };

  return new Promise((_) =>
    request(
      {
        url: `https://graph.facebook.com/v8.0/me/messages?access_token=${pageToken}`,
        method: "POST",
        json: true,
        headers: {
          "content-type": "application/json",
        },
        body,
      },
      function (error, response, body) {
        if (error) {
          console.error("--- reply =", JSON.stringify(error));
          return;
        }
        _(body);
      }
    )
  );
}

/**
 * {
 *  object: "page",
 *  entry: [{ id: "123123", time: 123123, messaging: [] }],
 * };
 * @see https://developers.facebook.com/docs/messenger-platform/discovery/facebook-chat-plugin
 * @see https://www.facebook.com/pageId: settings > message > messenger web plugin | auto reply
 * @see https://developers.facebook.com/apps/appId: callback url > add page > listen webhook
 */
async function handleFbListenHook(req, res) {
  const { entry } = req.body;
  console.log("--- fb hook =", entry.length);
  const { messaging } = entry[0];
  console.log("--- entry =", JSON.stringify(entry[0]));

  const { sender, recipient, message } = messaging[0];
  if (!message) {
    return;
  }
  const { text, tags } = message;
  console.log(
    sender,
    "-->",
    recipient,
    ":",
    text,
    ` (${JSON.stringify(tags)})`
  );
  const phone = detectPhone(text);
  if (phone) {
    console.log("--- nhan ra so dien thoai =", phone);
    const replyRes = await replyFbMessenger(
      sender,
      `Lin-shop sẽ sớm liên hệ với bạn qua số điện thoại ${phone} để xác nhận đơn hàng. Cảm ơn.`
    );
    console.log("-- reply =", replyRes);
  }

  res.status(200).end();
}

function detectPhone(text) {
  const beginPos = text.indexOf("0");
  let phone = "0";
  if (beginPos > -1) {
    for (let i = beginPos + 1; i < text.length; i++) {
      const isNum =
        ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"].find(
          (c) => c === text[i]
        ) !== undefined;
      if (isNum) {
        phone += text[i];
      } else {
        break;
      }
    }
  }

  return phone.length > 9 ? phone : null;
}
