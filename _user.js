const db = require("./_firebase");
const md5 = require("./_md5");

const clientSecret = "linssC";
const DEFAULT_EXPIRED = 15 * 60 * 1000; // 15 mins

module.exports = {
  apiRegistry,
  apiCheckExist,
  apiLogin,
  apiLogout,
  apiGetUserInfo,
  apiUpdateUserInfo,
  apiChangePassword,

  isAuthorize,
};

async function apiRegistry(req, res) {
  const { info } = req.body;
  const registryRes = await registry(info);

  if (registryRes) {
    res.status(200).end();
    return;
  }

  res.status(400).end();
}

async function apiCheckExist(req, res) {
  const { email } = req.body;
  const exist = await db.checkEmailExist(email);
  res.send(exist);
}

async function apiLogin(req, res) {
  const loginInfoEncoded = req.header("Authorization");
  const { userName, remember } = req.body;
  const loginRes = await login(userName, loginInfoEncoded, remember);
  const { error, access_token, userInfo } = loginRes;
  if (error) {
    res.status(400).end(error);
    return;
  }
  if (access_token) {
    res.send({ access_token, userInfo });
  }
}

async function isAuthorize(req) {
  const clientTokenEncoded = req.header("Authorization");
  const { email } = req.body;
  const token = await db.getTokenOf(email);
  if (!token) {
    return false;
  }
  const tokenEncoded = md5(`${token}:${clientSecret}`);
  return tokenEncoded === clientTokenEncoded;
}

async function apiLogout(req, res) {
  if (await isAuthorize(req)) {
    const { email } = req.body;
    const isRemoved = await db.removeTokenOf(email);
    if (isRemoved) {
      res.status(200).end();
      return;
    } else {
      res.status(400).end();
      return;
    }
  }

  res.status(401).end();
}

async function apiGetUserInfo(req, res) {
  if (await isAuthorize(req)) {
    const { email } = req.body;
    const userInfo = await db.getUserByEmail(email);
    delete userInfo.password;
    res.send(userInfo);
    return;
  }

  res.status(401).end();
}

async function apiChangePassword(req, res) {
  if (await isAuthorize(req)) {
    const { email, info } = req.body;
    const userInfo = await db.getUserByEmail(email);
    const { oldPassword, password } = info;
    if (oldPassword && password) {
      if (oldPassword === userInfo.password) {
        const newUserInfo = Object.assign({}, { ...userInfo }, { password });
        const updateRes = await db.updateUserByEmail(email, newUserInfo);
        if (updateRes) {
          const isRemoved = await db.removeTokenOf(email);
          if (isRemoved) {
            res.status(200).end();
          }
          res.status(400).end({ error: "Lỗi không xác định" });
          return;
        }
      } else {
        res.send({ error: "Sai mật khẩu" });
        return;
      }
    }
    res.send({ error: "Không đủ thông tin đổi mật khẩu" });
    return;
  }

  res.status(401).end();
}

async function apiUpdateUserInfo(req, res) {
  if (await isAuthorize(req)) {
    const { user, email } = req.body;
    delete user.email;
    const userInfo = await db.getUserByEmail(email);
    const newUserInfo = Object.assign({}, userInfo, user);
    const updateRes = await db.updateUserByEmail(email, newUserInfo);
    delete updateRes.password;
    res.send(updateRes);
  }

  res.status(401).end();
}

async function login(email, loginInfoEncoded, remember) {
  const userInfo = await db.getUserByEmail(email);
  if (!userInfo) {
    return { error: "Email đăng nhập không tồn tại" };
  }

  const userInfoEncoded = md5(`${userInfo.email}:${userInfo.password}`);
  if (userInfoEncoded === loginInfoEncoded) {
    const expired = remember ? null : new Date().valueOf() + DEFAULT_EXPIRED;
    const access_token = await db.genTokenFor(userInfo.email, expired);
    return {
      access_token,
      userInfo: { fullName: userInfo.fullName, email: userInfo.email },
    };
  }

  return { error: "Sai mật khẩu" };
}

async function registry(
  info = {
    email: "",
    password: "",
    checkPassword: "",
    fullName: "",
    phoneNumber: "",
    phoneNumberPrefix: "",
    agree: false,
  }
) {
  const res = await db.addUser(info);
  return res;
}
