#package.json | Procfile
#package-lock.json
#firebase

#!/bin/bash
COMMAND1=${1}
COMMAND2=${2}
COMMAND3=${3}

if [[ $COMMAND1 == 'build' ]]
then
    ng build --prod
fi

#copy config files
cp lin-shop-firebase-adminsdk*.json ../deploy/
cp CHANGELOG.md ../deploy/
cp new_version_email_template.html ../deploy/
#copy dependencies
cp package.json ../deploy/
cp package-lock.json ../deploy/
#copy back-end files
cp _*.js ../deploy/ -r
#copy front-end files
cp ./dist ../deploy/ -r
#deploy heroku
cd ../deploy
git add .
git commit -am "deploy"
git push heroku master

cd ../lin-shop
node _increase-version.js

sleep 10
cd ../deploy

if [[ $COMMAND1 == 'open' || $COMMAND2 == 'open' || $COMMAND3 == 'open' ]]
then
    start https://lin-shop2.herokuapp.com
fi

if [[ $COMMAND1 == 'log' || $COMMAND2 == 'log' || $COMMAND3 == 'log' ]]
then
    heroku logs --tail --app=lin-shop2
fi