const isProd = !!process.env.host;
const isWriteLog = process.env.log === "true";
const host = process.env.host || "http://localhost";
const port = process.env.PORT || 80;
const ng_port = 4200;

console.log("Log mode    =", isWriteLog);
console.log("Environment =", isProd ? "PRODUCT" : "DEV");
console.log("______________________________\n");

const root = __dirname + "/dist/lin-shop";
const API = "/api";

// ======================================== SERVER ========================================

const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

const api = require("./_api").config({
  root,
  host: isProd ? host : `${host}:${ng_port}`,
  isWriteLog,
});
const user = require("./_user");
const fb = require("./_fb-messenger");
const rss = require("./_rss");
const mailchimp = require("./_mailchimp").config({ root: __dirname });

var app = express();
app.use(cors()); // To allow angular call nodejs
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// version
app.get(`${API}/version`, api.getVersion);
// users
app.post(`${API}/users/exist`, user.apiCheckExist);
app.post(`${API}/users/signup`, user.apiRegistry);
app.post(`${API}/users/login`, user.apiLogin);
app.post(`${API}/users/logout`, user.apiLogout);
app.post(`${API}/users/info`, user.apiGetUserInfo);
app.put(`${API}/users/info`, user.apiUpdateUserInfo);
app.put(`${API}/users/change-password`, user.apiChangePassword);
// items
app.get(`${API}/categories/:category`, api.getItemsByCategory);
app.get(`${API}/items`, api.getItemsByName);
app.get(`${API}/items/:id`, api.getItemById);
// payment
app.post(`${API}/payment`, api.handleGetBillInfo);
app.post(`${API}/payment/cod`, api.handlePayCodCheckout);
app.put(`${API}/payment/cod`, api.handlePayCodCapture);
app.post(`${API}/payment/paypal`, api.handlePaypalCheckout_v2);
app.put(`${API}/payment/paypal`, api.handlePaypalCapture);
app.post(`${API}/payment/ngan-luong`, api.handleNganLuongCheckout_v3);
app.put(`${API}/payment/ngan-luong`, api.handleNganLuongCapture);
// rss
app.get(`${API}/rss/vnexpress`, rss.getVnexpress);
app.get(`${API}/rss-parse/vnexpress`, rss.parseVnexpress);
app.get(`${API}/rss/24h`, rss.get24h);
app.get(`${API}/rss-parse/24h`, rss.parse24h);
// facebook
app.get(`/hook/fb`, fb.handleFbAddHook);
app.post("/hook/fb", fb.handleFbListenHook);
// google
// @see https://search.google.com/search-console
// http://www.google.com/ping?sitemap=https://example.com/sitemap.xml to tell google get sitemap
app.get(`/robots.txt`, api.sendRobotsTxt);
app.get(`/sitemap.xml`, api.sendSitemapXml);
// mailchimp
app.get(`${API}/mailchimp/members`, mailchimp.apiGetMembers);
app.put(`${API}/mailchimp/members`, mailchimp.apiUpdateMembers);
app.get(`${API}/mailchimp/send-all`, mailchimp.apiGetStatus);
app.put(`${API}/mailchimp/send-all`, mailchimp.apiSendAll);
// twilio
app.post(`${API}/mailchimp/send-all`, mailchimp.apiRequestCode);
// default
app.get("/", api.handleDefaultTemplate);
app.use(express.static(root)).use(api.handleDefaultTemplate);

const db = require("./_firebase");
db.getVersion().then((version) => {
  api.setVersion(version);
  app.listen(port, () => api.handleAppListen(port));
});

// WARN: heroku will stop & re-start many time per day
// so version will increase
// Mailchimp report abuse
// if (isProd) {
//   mailchimp.notifyNewVersion();
// }
