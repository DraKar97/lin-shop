var firebase = require("firebase-admin");
var serviceAccount = require("./lin-shop-firebase-adminsdk-3l74x-9e8b68cd27.json");

module.exports = {
  addUser,
  checkEmailExist,
  genTokenFor,
  getTokenOf,
  getUserByEmail,
  updateUserByEmail,
  removeTokenOf,

  getExchange,
  getItemById,
  getItemsByName,
  getItemsByCategory,
  getBill,
  addBill,
  updateBill,

  changeSubscriber,

  getVersion,
  increaseVersion,
  getTime,
};

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://lin-shop.firebaseio.com/",
});

var database = firebase.database();
var db = database.ref();

async function readAll() {
  return await db.once("value").then((res) => res.val());
}

async function getExchanges() {
  return await db
    .child("exchanges")
    .once("value")
    .then((res) => res.val());
}

async function getExchange(currencyCode) {
  return await db
    .child(`exchanges/${currencyCode}`)
    .once("value")
    .then((res) => res.val());
}

async function getItems() {
  return await db
    .child("categories")
    .once("value")
    .then((res) => res.val())
    .then((categoriesObj) =>
      Object.keys(categoriesObj)
        .map((category) => {
          let categoryObj = categoriesObj[category];
          Object.keys(categoryObj).forEach((itemId) => {
            categoryObj[itemId].category = category;
          });
          return categoryObj;
        })
        .reduce((a, b) => ({ ...a, ...b }))
    );
}

async function getItemsByName(name, category = "") {
  const _name = name.toLowerCase().trim().replace(/\s\s+/g, " ");
  let items;
  if (category) {
    items = await getItemsByCategory(category);
  } else {
    items = await getItems();
  }

  let result = [];
  for (let id in items) {
    const matchName = items[id].name.toLowerCase().indexOf(_name) > -1;
    const matchId = id.replace(/\-/g, " ").indexOf(_name) > -1;
    if (matchName || matchId) {
      result.push({ ...items[id], id });
    }
  }
  return result;
}

async function getItemsByCategory(category) {
  return await db
    .child(`categories/${category}`)
    .once("value")
    .then((res) => res.val());
}

async function getItemById(id) {
  const itemsObj = await getItems();
  return itemsObj[id];
}

async function getBills() {
  return await db
    .child("bills")
    .once("value")
    .then((res) => res.val());
}

async function addBill(method, id, cart, billInfo, buyerInfo, status) {
  const isExist = (await getBill(id)) !== null;
  if (isExist) {
    // TODO: handle error
    return;
  }

  const bill = {
    method,
    cart,
    billInfo,
    status,
    buyerInfo,
    modify: getTime().toLocaleString(),
  };

  return await db
    .child("bills")
    .child(id)
    .set(bill)
    .then(() => ({ [id]: bill }));
}

async function getBill(id) {
  return await db
    .child(`bills/${id}`)
    .once("value")
    .then((res) => res.val());
}

async function updateBill(id, status) {
  const oldBill = await getBill(id);
  if (oldBill === null) return false;

  const newBill = { ...oldBill, status, modify: getTime().toLocaleString() };
  return await db
    .child(`bills/${id}`)
    .update(newBill)
    .then(() => newBill);
}

async function getSubscriber(email) {
  const emailEncode = email.replace(".", "_dot_");

  return await db
    .child("subscribers")
    .child(emailEncode)
    .once("value")
    .then((e) => e.val());
}

function emailEncoder(email) {
  return (email || "").replace(".", "_dot_");
}

async function changeSubscriber(userInfo) {
  const emailEncode = emailEncoder(userInfo.email_address);

  if (getSubscriber(userInfo.email_address)) {
    return await db
      .child("subscribers")
      .child(emailEncode)
      .set({ ...userInfo, modify: getTime().toLocaleString() });
  }

  return await db
    .child("subscribers")
    .child(emailEncode)
    .set({ ...userInfo, create: getTime().toLocaleString() });
}

async function getUsers() {
  return await db
    .child("users")
    .once("value")
    .then((e) => e.val())
    .then((obj) => Object.keys(obj || {}).map((key) => obj[key]));
}

async function getUserByEmail(email) {
  const users = await getUsers();
  const user = users.find((m) => m.email === email);
  return user;
}

async function updateUserByEmail(email, userInfo) {
  const emailEncode = emailEncoder(email);

  const res = await db
    .child(`users/${emailEncode}`)
    .set(userInfo)
    .then(() => userInfo);
  return res;
}

async function checkEmailExist(email) {
  const exist = await getUserByEmail(email);
  return !!exist;
}

async function addUser(info) {
  const { email } = info;
  const user = await getUserByEmail(email);

  if (!user) {
    const res = await db
      .child("users")
      .child(emailEncoder(email))
      .set(info)
      .then(() => info);
    return res;
  }
}

async function genTokenFor(email, expired) {
  const emailEncode = emailEncoder(email);
  const token = generateToken();
  const tokenObj = {
    token,
    expired,
  };
  const res = await db
    .child(`tokens/${emailEncode}`)
    .set(tokenObj)
    .then(() => token);
  return res;
}

async function getTokenOf(email) {
  const emailEncode = emailEncoder(email);
  const res = await db
    .child(`tokens/${emailEncode}`)
    .once("value")
    .then((e) => e.val())
    .then(async (tokenObj) => {
      if (!tokenObj) {
        return null;
      }
      if (tokenObj.expired && tokenObj.expired < new Date().valueOf()) {
        await removeTokenOf(email);
        return null;
      }
      return tokenObj.token;
    });
  return res;
}

function generateToken() {
  return `ls-${Math.round(Math.random() * 1000000)}`;
}

async function removeTokenOf(email) {
  const emailEncode = emailEncoder(email);
  let hasError;
  const res = await db
    .child(`tokens/${emailEncode}`)
    .remove((error) => {
      if (error) {
        console.error("remove token =", error);
        hasError = error;
      }
    })
    .then(() => (hasError ? false : true));
  return res;
}

async function getVersion() {
  return await db
    .child("version")
    .once("value")
    .then((e) => e.val());
}

async function increaseVersion() {
  const currentVersion = await getVersion();
  const newVersion = (currentVersion * 100 + 1) / 100;
  return await db
    .child("version")
    .set(newVersion)
    .then(() => newVersion);
}

function getTime() {
  let now = new Date();
  now.setTime(now.getTime() + 7 * 60 * 1000);
  return now;
}
