const http = require("./_request");
const utils = require("./_utils");

module.exports = {
  sendSms,
};

const id = "ACa45a5645734ba79128ce299071dc9161";
const secret = "52629c718bc467020e5ea22673756eb3";
const auth = "Basic " + Buffer.from(`${id}:${secret}`).toString("base64");
const phone = "+15038280775";

const host = "api.twilio.com";
const path = "2010-04-01/Accounts";
const PATH_SMS = `${path}/${id}/Messages.json`;

// @see https://www.twilio.com/docs/sms/api

async function getToken() {
  const res = await http.get2(`https://${host}/${path}`, auth);
  const token = await utils.parseXmlToJson(res).then(reduceXml);
  return token;
}

function reduceXml(res) {
  return res.TwilioResponse.Accounts[0].Account[0].AuthToken[0];
}

async function sendSms(Body) {
  const res = await http
    .post2(`https://${host}/${PATH_SMS}`, auth, {
      Body,
      From: phone,
      To: "+84947165149",
    })
    .then((e) => JSON.parse(e));

  const error = res.message || res.error_message;

  if (error) {
    console.log("--- send sms =", error);
    return { error };
  }
  console.log("--- send sms =", res.status);
  return { status: res.status };
}
