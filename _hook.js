// @see https://developer.paypal.com/docs/api/webhooks/v1/#webhooks_post
// Out of date

var host;
var API;

module.exports = {
  config,
  configHookForProduct,
};

const URL_HOOK = "/v1/notifications/webhooks";

/** Config is in progress */
var inProgress = {};

const http = require("./_request");
const paypal = require("./_paypal");

function getHooks(callback) {
  inProgress["getHooks"] = true;
  http.get(URL_HOOK, ({ webhooks }) => {
    inProgress["getHooks"] = false;
    const hooks = webhooks.map((hook) => hook.id);
    callback(hooks);
  });
}

function deleteHook(id, callback) {
  http.delete(`${URL_HOOK}/${id}`, callback);
}

function deleteHooks(hooks) {
  if (!hooks.length) return;

  hooks.forEach((hook) => {
    console.log("------ deleting hook =", hook);
    inProgress[hook] = true;
    deleteHook(hook, (res, statusCode) => {
      console.log(`------ deleting hook [${hook}] = ${statusCode}`);
      inProgress[hook] = false;
    });
  });
}

function addOneHook(url, event = "CHECKOUT.ORDER.APPROVED") {
  // Reg hook for url, use this code or config at dev.paypal
  const data = JSON.stringify({
    url,
    event_types: [
      {
        name: event,
      },
    ],
  });
  http.post(URL_HOOK, data, ({ id }) => {
    console.log(`--- Add hook [${event}] =`, id || "ERROR");
    if (!id) return;

    http.get(`${URL_HOOK}/${id}`, ({ event_types }) => {
      if (!event_types) return;
      console.log(
        `--- Listen hook [${event}] =`,
        (event_types[0] || { status: "ERROR" }).status,
        url
      );
    });
  });
}

function listenAddHook(intervalInstance) {
  let isDone = true;
  for (let id in inProgress) {
    if (inProgress[id]) {
      isDone = false;
      break;
    }
  }

  if (isDone) {
    const eventName = "CHECKOUT.ORDER.APPROVED";
    const url = `${host}${API}/hook`;
    console.log(`--- Reg hook [${eventName}] for ${url}`);
    addOneHook(url);
    clearInterval(intervalInstance);
  }
}

/**
 * Auto config hook, or config hook manually
 * @see https://developer.paypal.com/developer/applications/
 * > SANDBOX WEBHOOKS
 */
function configHookForProduct(callback) {
  paypal.getToken(({ access_token }) => {
    http.setToken(access_token);
    console.log("--- access_token =", access_token);

    console.log("--- delete all hooks =");
    getHooks(deleteHooks);

    // Add new hook
    let intervalInstance = setInterval(
      () => listenAddHook(intervalInstance),
      200
    );
  });
}

function config(v) {
  host = v.host;
  API = v.API;
  return this;
}
